import { Injectable, PipeTransform } from '@angular/core';

@Injectable()
export class NgsDescConfig {
}

export declare type textFunc = (data: any) => string;

export class NgsDescOption {
    dataSource: any;
    descItemOption!: Array<NgsDescItemOption>;
}

export class NgsDescItemOption {
    key:string | any;
    value?:string | any;
    info?:textFunc;
    hidden?: (data: any) => boolean;
}
