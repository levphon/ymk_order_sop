import { Component, Input, OnInit } from '@angular/core';
import { NgsDescItemOption, NgsDescOption } from './ngs-desc.config';

@Component({
  selector: 'ngs-desc',
  templateUrl: './ngs-desc.html',
  styleUrls: ['./ngs-desc.css']
})
export class NgsDesc implements OnInit {
  router: any;

  constructor() { }

  @Input()
  option!: NgsDescOption;

  ngOnInit() {
  }

  getText(option: NgsDescItemOption, rootOption: NgsDescOption): string {
    if(rootOption.dataSource){
      if (option.info) {
        return option.info(rootOption.dataSource);
      } else {
        return rootOption.dataSource[option.value];
      }
    }
    return "";
  }

}
