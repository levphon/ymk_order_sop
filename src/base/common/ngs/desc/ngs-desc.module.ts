import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule,DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';

import { NgsDesc } from './ngs-desc';
import {
	NgsDescOption, NgsDescConfig
} from './ngs-desc.config';

export { NgsDesc } from './ngs-desc';
export {
	NgsDescOption, NgsDescConfig
} from './ngs-desc.config';

const NGB_TABSET_DIRECTIVES = [NgsDesc];

@NgModule({ 
    declarations: NGB_TABSET_DIRECTIVES, 
    exports: NGB_TABSET_DIRECTIVES, 
    imports: [
        CommonModule,
        FormsModule,
        NzDescriptionsModule
    ] 
})
export class NgsDescModule {
	static forRoot(): ModuleWithProviders<any> { return { ngModule: NgsDescModule, providers: [NgsDescConfig,DatePipe] }; }
}
