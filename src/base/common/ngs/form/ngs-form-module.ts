import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzCascaderModule } from 'ng-zorro-antd/cascader';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzModalModule } from 'ng-zorro-antd/modal';

import { NgsForm } from './form/ngs-form';
import {
    NgsFormConfig, NgsFormOption, NgsFormCompOption, NgsFormInputCompOption,
    NgsFormTextareaCompOption, NgsFormRadioCompOption, NgsFormCheckboxCompOption, NgsFormSelectCompOption,
    NgsFormDatePickerCompOption,NgsFormCascaderCompOption
} from './ngs-form.config';



export { NgsForm } from './form/ngs-form';
export {
    NgsFormConfig, NgsFormOption, NgsFormCompOption, NgsFormInputCompOption,
    NgsFormTextareaCompOption, NgsFormRadioCompOption, NgsFormCheckboxCompOption, NgsFormSelectCompOption,
    NgsFormDatePickerCompOption,NgsFormCascaderCompOption
} from './ngs-form.config';


const NGB_TABSET_DIRECTIVES = [NgsForm];

@NgModule({
  declarations: NGB_TABSET_DIRECTIVES,
  exports: NGB_TABSET_DIRECTIVES,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzDropDownModule,
    NzDividerModule,
    NzSelectModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzIconModule,
    NzDatePickerModule,
    NzRadioModule,
    NzCheckboxModule,
    NzCascaderModule,
    NzInputNumberModule,
    NzModalModule
  ]
})

export class NgsFormModule {
  static forRoot(): ModuleWithProviders<any> { return { ngModule: NgsFormModule, providers: [NgsFormConfig] }; }
}