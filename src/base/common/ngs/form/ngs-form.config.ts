import { Injectable } from "@angular/core";
import { FormGroup, ValidatorFn } from "@angular/forms";
import { CmdData } from "src/core/module/CmdSignModel";

@Injectable()
export class NgsFormConfig {
	uploaderConfig: NgsFormUploaderConfig | any;
}

export type wrapperUploadDataFunc = (data: any) => void;
export type uploadSuccessFunc = (data: any) => void;
export type uploadBeforeSendFunc = (block: any, data: any, headers: any) => void;
export declare type textFunc = (data: any) => string;
/**上传配置 */
export class NgsFormUploaderConfig {
	server: string | any;//服务器地址
	wrapperUploadData?: wrapperUploadDataFunc;
	uploadBeforeSend?: uploadBeforeSendFunc;//错误处理
	uploadSuccess?: uploadSuccessFunc;//上传完成
}

export interface NgsFormOption {
	labelSpan?: number;
	compSpan?: number;
	gutter?: number;
	components: Array<NgsFormCompOption | NgsFormInputCompOption | NgsFormSelectCompOption | NgsFormTextareaCompOption | NgsFormRadioCompOption | 
	NgsFormUploaderCompOption | NgsFormUmeditorCompOption | NgsFormDatePickerCompOption | NgsFormCheckboxCompOption | NgsFormCascaderCompOption >;
	showSearch?: boolean;
	column?: number;//最大列数量
}


export type onChangeFunc = (option: NgsFormCompOption, valueObj?: any) => void;
/**公共类 */
export interface NgsFormCompOption {
	comp: "NgsFormInput" | "NgsFormSelect" | "NgsFormTextarea" | "NgsFormRadio" | "NgsFormUploader" | 
			"NgsFormUmeditor" | "NgsFormDatePicker" | "NgsFormCheckbox" | "NgsFormCascader";
	label: string | any;
	property: string;
	require?:boolean | any;
	span?: number;//组件span
	labelSpan?: number;//组件标签span
	compSpan?: number;//组件输入组件span
	validations?: Array<NgsFormValidationOption>;
	onChange?: onChangeFunc;
	hidden?: boolean;
	disabled?: boolean;
	formOption?: NgsFormOption;
}

/**校验类型 */
export interface NgsFormValidationOption {
	property?: string;
	msg: string | any;
	type: string | any;
	fn: ValidatorFn | any;
}

/**搜索 */
export interface NgsFormSearchOption extends NgsFormCompOption {
	offset?: number;
	hideReset?: boolean;
}

/**输入框 */
export interface NgsFormInputCompOption extends NgsFormCompOption {
	type: 'text' | 'password' | 'number';
	placeHolder?: string;
	maxLength?: number;
	disabled?: boolean;
}
/**文本域 */
export interface NgsFormTextareaCompOption extends NgsFormCompOption {
	height: number | any;
}
/**单选框 */
export interface NgsFormRadioCompOption extends NgsFormCompOption {
	dataSource?: Array<Map<string,any>>;
	dsLabel?: string;
	dsValue?: string;
}

/**多选框 */
export interface NgsFormCheckboxCompOption extends NgsFormCompOption {
	dataSource?: any;
	data?: Array<any> | any;
	dsLabel?: string;
	dsValue?: string;
}

/**下拉框 */
export interface NgsFormSelectCompOption extends NgsFormCompOption {
	dataSource?: CmdData;
	data?:any;
	dsLabel?: string;
	dsValue?: string;
	dsSubLabel?: string,
	dsSubValue?: string | textFunc,
	model?: 'default' | 'tags' | 'multiple';
	placeHolder?: string;
	openSerch:boolean;
}
/**日期选择 */
export interface NgsFormDatePickerCompOption extends NgsFormCompOption {
	showTime?: boolean;
	format?: string;
	modelPicker ?:'date' | 'range';
}

/**级联*/
export interface NgsFormCascaderCompOption extends NgsFormCompOption {
	dataSource?: Array<Map<string,any>>;
	dsLabel?: string;
	dsValue?: string;
}

/**富文本 */
export interface NgsFormUmeditorCompOption extends NgsFormCompOption {
	config?: any; //umeditor配置项
	path?: string;//umeditor代码根目录路径，以 / 结尾
	loadingTip?: string;//初始化提示文本
	setting?: any;
}

export type errHandlerFunc = (err: any) => void;

export interface NgsFormUploaderCompOption extends NgsFormCompOption {
	accept?: 'image' | 'video' | 'file';
	multiple?: boolean;
	limit?: number;
	uploaderId?: string;//默认picker
	errHandler?: errHandlerFunc;//错误处理
	width?: number;
	height?: number;
	data: Array<any>;
}