import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzIconModule } from 'ng-zorro-antd/icon';


import {
  NgsTableConfig, NgsTableOption, NgsTabSelectionsOption, NgsTableDataOption,
  NgsTableColumnOption, NgsTableOpOption, NgsTableOpGroupOption,
  NgsTableOpBtnOption, NgsTabColumnBadgeOption,NgsTabColumnSubOption,
  NgsTableTagOption
} from './ngs-table.config';

import { NgsTable } from './tab/ngs-table';
import { NgsTableColumn } from './column/ngs-table-column';

export { NgsTable } from './tab/ngs-table';
export { NgsTableColumn } from './column/ngs-table-column';

export {
  NgsTableConfig, NgsTableOption, NgsTabSelectionsOption, NgsTableDataOption,
  NgsTableColumnOption, NgsTableOpOption, NgsTableOpGroupOption,
  NgsTableOpBtnOption, NgsTabColumnBadgeOption,NgsTabColumnSubOption,
  NgsTableTagOption

} from './ngs-table.config';


const NGB_TABSET_DIRECTIVES = [NgsTable, NgsTableColumn];

@NgModule({
  declarations: NGB_TABSET_DIRECTIVES,
  exports: NGB_TABSET_DIRECTIVES,
  imports: [
    CommonModule,
    FormsModule,
    NzToolTipModule,
    NzTableModule,
    NzDropDownModule,
    NzDividerModule,
    NzSelectModule,
    NzTabsModule,
    NzBadgeModule,
    NzFormModule,
    NzTagModule,
    NzIconModule
  ]
})

export class NgsTableModule {
  static forRoot(): ModuleWithProviders<any> { return { ngModule: NgsTableModule, providers: [NgsTableConfig] }; }
}