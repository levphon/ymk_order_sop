import { animate, AnimationBuilder, style } from '@angular/animations';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from 'src/base/utlis/http.config';
import { CmdData, CmdSignModel } from 'src/core/module/CmdSignModel';
import { CoreService } from 'src/core/service/core.service';
import { NgsTableOpBtnOption, NgsTableOption } from '../ngs-table.config';


@Component({
  selector: 'ngs-table',
  templateUrl: './ngs-table.html',
  styleUrls: ['./ngs-table.scss']
})

export class NgsTable implements OnInit {

  constructor(
    private router: Router,
    private http:HttpService,
    private coreService: CoreService,
    private animBuilder: AnimationBuilder
  ) {

  }

  @ViewChild('tabBar',{static:true}) tabBar: ElementRef | any;
  @Input()
  option!: NgsTableOption;

  selectElemWidth: number = 0;
  selectIndex: number = 0;
  @ViewChildren('tabComp') tabCompArray: QueryList<ElementRef> | any;
  @Output() ngdsTabSelect: EventEmitter<any> = new EventEmitter();
  
  data: Array<any> = [];
  cmdData:CmdData|any;
  form:any ={}
  page: any = {
    total: 0,
    pageIndex: 1,
    size: 10,
    show: true
  }

  loading: boolean = false;

  pageSizeOption: number[] = [5, 10, 20, 30, 40, 50];


  ngOnInit(): void {
    this.cmdData = this.option.dataSource;
    this.init();
  }

  init(){
    this.loading = true;
    let cmdSign = new CmdSignModel();
    cmdSign.cmdCode = this.cmdData.cmdCode;
    if(this.cmdData.source){
      Object.assign(this.cmdData.source, this.form);
      this.cmdData.source = this.cmdData.source;
    }else{
      this.cmdData.source = this.form;
    }
    cmdSign.source = this.cmdData.source;
    cmdSign.returnStruct = this.cmdData.returnStruct;
    cmdSign.pageIndex = this.page.pageIndex;
    cmdSign.pageSize = this.page.size;
    cmdSign.pageTotal = this.page.total;

    this.http.exe(cmdSign).then(res => {
      this.data = res.source;
      this.page.total = res.pageTotal;
      this.loading = false;
    });
  }

  search(event:any){
    if(this.form && this.form.status){
      event.status = this.form.status;
      this.form = event;
    }else{
      this.form = event;
    }
    this.init();
  }

  /**
  * 按钮文案
  */
  getBtnText(col: NgsTableOpBtnOption, item: any) {
    if (typeof col.text === "function") {
      return col.text(item);
    } else {
      return col.text;
    }
  }



  /**
  * 页码更改事件
  */

  indexChange(event:number) {
    this.page.pageIndex = event;
    this.init();
  }

  /**
  * 每页数量更改事件
  */
  pageSizeChange(event:number) {
    this.page.pageIndex = event;
    this.init();
  }

  selectChange($event:any){
    this.form.status = $event;
    this.init();
  }

  btnClick(option:NgsTableOpBtnOption,event:any){
    option.action(event);
  }

  


}