import { Component, Input, OnInit } from '@angular/core';

import { Router } from '@angular/router';

import { CoreService } from 'src/core/service/core.service';

import { NgsTabColumnBadgeOption, NgsTabColumnSubOption, NgsTableColumnOption, NgsTableTagOption } from '../ngs-table.config';



@Component({
  selector: 'ngs-table-column',
  templateUrl: './ngs-table-column.html',
  styleUrls: ['./ngs-table-column.css']
})

export class NgsTableColumn implements OnInit {


  constructor(
    private router: Router,
    private coreService: CoreService
  ) {

  }

  @Input() colOption: NgsTableColumnOption | any;
  @Input() item: any;


  ngOnInit(): void {
  }

  /**
  
  * 获取文案
  * @param option
  * @returns
  */
  getColunmText(option: NgsTableColumnOption) {

    if (option.info) {
      return option.info(this.item);
    } else {
      return this.item[option.property];
    }
  }

  /**
   *  获取tag
   */
  getTagText(option: NgsTableTagOption){
    
    if (option.tagLabel instanceof Function) {
      return option.tagLabel(this.item);
    } else {
      return option.tagLabel;
    }
  }

  /**
   * getTagColor
   */
   getTagColor(option: NgsTableTagOption):string{
    if (option.tagColor instanceof Function) {
      return option.tagColor(this.item);
    } else {
      return option.tagColor;
    }
   }
  /**
   * 获取sub文本
   */
   getSubText(option: NgsTableColumnOption){
    if (option instanceof Function) {
      return option(this.item);
    } else {
      return option;
    }
   }

  getBadgeColor(option: NgsTableColumnOption):any {
    let color = "";
    this.colOption.badgeOption.forEach((element:NgsTabColumnBadgeOption) => {
      if(element.status == this.item[option.property]){
        color = element.tagColor;
      }
    });
    return color;
  }

  getBadgeText(option: NgsTableColumnOption):any {
    let test = "";
    this.colOption.badgeOption.forEach((element:NgsTabColumnBadgeOption) => {
      if(element.status == this.item[option.property]){
        test = element.tagLabel;
      }
    });
    return test;
  }

  ColumnStyle = {
    width:"120px"
  }
  getColumnStyle(option:NgsTableColumnOption):any{
    let restlt:any;
    if(option.width){
      restlt = {
        width:option.width + "px"
      }
    }
    return restlt;
  }

}