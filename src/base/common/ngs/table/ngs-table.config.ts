import { Injectable } from "@angular/core";
import { CmdData } from "src/core/module/CmdSignModel";

@Injectable()
export class NgsTableConfig {
}


export declare type pipeFunc = (property: string, data: any) => string | boolean;
export declare type clickFunc = (item: any) => void;
export declare type textFunc = (data: any) => string;
export declare type permFunc = (data: any) => string;
export declare type onSelect = () => void;
export declare type loadingFunc = (data: any) => boolean;
export type expandChange = (item: any, extend: any) => void;

/**表主结构 */
export interface NgsTableOption {
  dataSource: CmdData;
  table:NgsTableDataOption;
  showCheck?: boolean;
  selections?: Array<NgsTabSelectionsOption>;
  expandChange?: expandChange;
}

/**状态栏 */
export interface NgsTabSelectionsOption {
  text: string;
  value:any;
}

/**表结构 */
export interface NgsTableDataOption {
  isComplexPage?: boolean;
  columns: Array<NgsTableColumnOption>;
  op?: NgsTableOpOption;
}

/**列结构 */
export interface NgsTableColumnOption {
  title: string;
  property: string;
  width?: string | any;
  info?: textFunc;
  click?: clickFunc;
  badgeOption?:Array<NgsTabColumnBadgeOption>;
  tagOptin?:Array<NgsTableTagOption>;
  subOption?:string | textFunc;
}
/**标签 */
export interface NgsTableTagOption{
  hidden?: (data: any) => boolean;
  tagColor: textFunc | any;
  tagLabel: textFunc | string;
}
/**状态 */
export interface NgsTabColumnBadgeOption {
  status: number | any;
  tagColor: string | any;
  tagLabel: string | any;
}
/**小文字 */
export interface NgsTabColumnSubOption {
  text: string | textFunc;
}


/** 按钮主结构 */
export interface NgsTableOpOption {
  width?: string | any;
  buttons?: Array<NgsTableOpBtnOption>;
  groupButtons?: Array<NgsTableOpGroupOption>;
}

/**按钮分组结构 */
export interface NgsTableOpGroupOption {
  text: string | textFunc;
  buttons: Array<NgsTableOpBtnOption>;
  hidden?: (data: any) => boolean;
}

/**按钮结构 */
export interface NgsTableOpBtnOption {
  text: string | textFunc;
  action: (data: any) => void | Promise<any>;
  hidden?: (data: any) => boolean;

}