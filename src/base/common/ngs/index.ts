import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzIconModule } from 'ng-zorro-antd/icon';


import { NgsTableModule } from './table/ngs-table-module';
export {
    NgsTableConfig, NgsTableOption, NgsTabSelectionsOption, NgsTableDataOption,
    NgsTableColumnOption, NgsTableTagOption, NgsTableOpOption, NgsTableOpGroupOption,
    NgsTableOpBtnOption

} from './table/ngs-table-module';


import { NgsPanelModule } from './panel/panel.module';
export {
    NgsPanelModule,
    NgsPanel,
    NgsPanelConfig, NgsPanelOption, NgsPanelCrumbsOption
} from './panel/panel.module';


import { NgsFormModule } from './form/ngs-form-module';
export {
    NgsFormConfig, NgsFormOption, NgsFormCompOption, NgsFormInputCompOption,
    NgsFormTextareaCompOption, NgsFormRadioCompOption, NgsFormCheckboxCompOption, NgsFormSelectCompOption,
    NgsFormDatePickerCompOption, NgsFormCascaderCompOption
} from './form/ngs-form-module';


import { NgsBlockModule } from './block/ngs-block-module';
export {
    NgsBlockConfig, NgsBlockOption
} from './block/ngs-block-module';


import { NgsEditorModule } from './editor/editor.module';

import { NgsDescModule } from './desc/ngs-desc.module';
export {
    NgsDescConfig, NgsDescOption
} from './desc/ngs-desc.module';

const DIRECTIVES = [NgsTableModule, NgsPanelModule, NgsFormModule, NgsBlockModule,NgsEditorModule,NgsDescModule]

@NgModule({
    exports: DIRECTIVES,
    imports: [
        CommonModule,
        NzTableModule,
        NzTagModule,
        NzToolTipModule,
        NzImageModule,
        NzEmptyModule,
        NzBadgeModule,
        NzAvatarModule,
        NzDividerModule,
        NzTabsModule,
        NzDropDownModule,
        NzIconModule,
        NgsTableModule.forRoot(),
        NgsPanelModule.forRoot(),
        NgsFormModule.forRoot(),
        NgsBlockModule.forRoot(),
        NgsDescModule.forRoot()
    ],
    providers: []
})
export class NgsRootModule {
}

@NgModule({ imports: DIRECTIVES, exports: DIRECTIVES })
export class NgsModule {
    static forRoot(): ModuleWithProviders<any> { return { ngModule: NgsRootModule }; }
}