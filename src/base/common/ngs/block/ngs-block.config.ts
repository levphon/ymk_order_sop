import { Injectable, PipeTransform } from '@angular/core';

@Injectable()
export class NgsBlockConfig {
}

export class NgsBlockOption {
    title: string | any;
    buttons?: Array<NgsBlockBtnOption>;
}
export interface NgsBlockBtnOption {
    text: string;
    action: (data: any) => void;
}