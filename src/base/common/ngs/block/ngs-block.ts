import { Component, Input, OnInit } from '@angular/core';
import { NgsBlockOption } from './ngs-block.config';

@Component({
    selector:'ngs-block',
    templateUrl: './ngs-block.html',
    styleUrls: ['./ngs-block.scss']
})
export class NgsBlock implements OnInit {
  router: any;

  constructor() { }

  @Input() option: NgsBlockOption | any;
  @Input()
  title!: string;

  ngOnInit() {
  }

  getText(value:string){
    return value?value:this.title;
  }

}
