import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule,DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NzCardModule } from 'ng-zorro-antd/card';

import { NgsBlock } from './ngs-block';
import {
	NgsBlockConfig, NgsBlockOption
} from './ngs-block.config';

export { NgsBlock } from './ngs-block';
export {
	NgsBlockConfig, NgsBlockOption
} from './ngs-block.config';

const NGB_TABSET_DIRECTIVES = [NgsBlock];

@NgModule({ 
    declarations: NGB_TABSET_DIRECTIVES, 
    exports: NGB_TABSET_DIRECTIVES, 
    imports: [
        CommonModule,
        FormsModule,
        NzCardModule
    ] 
})
export class NgsBlockModule {
	static forRoot(): ModuleWithProviders<any> { return { ngModule: NgsBlockModule, providers: [NgsBlockConfig,DatePipe] }; }
}
