import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzFormModule } from 'ng-zorro-antd/form';

import { NgsEditor } from './editor';
export { NgsEditor } from './editor';

const NGB_TABSET_DIRECTIVES = [NgsEditor];

@NgModule({
  declarations: NGB_TABSET_DIRECTIVES,
  exports: NGB_TABSET_DIRECTIVES,
  imports: [
    CommonModule,
    FormsModule,
    NzToolTipModule,
    NzTableModule,
    NzDropDownModule,
    NzDividerModule,
    NzSelectModule,
    NzTabsModule,
    NzBadgeModule,
    NzFormModule
  ]
})

export class NgsEditorModule {

}