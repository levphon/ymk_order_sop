import {
    Component,
    AfterContentChecked,
    Input,
    OnInit
} from '@angular/core';
import {NgsPanelConfig, NgsPanelOption, NgsPanelBtnOption} from './panel.config';

/**
 * A component that makes it easy to create tabbed interface.
 */
@Component({
    selector: 'ngs-panel',
    exportAs: 'ngs-Panel',
    styleUrls: ['./panel.scss'],
    template: `
        <div style="width:100%;">
            <div class="ngs-panel">
                <div class="ngs-panel-div">
                    <nz-breadcrumb>
                        <nz-breadcrumb-item *ngFor="let crumb of option.crumbs;">
                            <a (click)="crumb.action&&crumb.action()" *ngIf="crumb.action" class="ngs-panel-a">{{crumb.text}}</a>
                            <span *ngIf="!crumb.action"  class="ngs-panel-a">{{crumb.text}}</span>
                        </nz-breadcrumb-item>
                    </nz-breadcrumb>
                </div>
                <div>
                    <a *ngFor="let btn of option.buttons;"
                            (click)="btn.action()" [hidden]="btn.hidden">
                        {{btn.text}}
                    </a>
                </div>
            </div>
            <div class="ngs-panel-body">
                <ng-content></ng-content>
            </div> 
        </div>
    `
})
export class NgsPanel implements OnInit {
    constructor() {
    }

    @Input()
    option!: NgsPanelOption;

    ngOnInit() {

    }

    getBtnStyle = function (btn: NgsPanelBtnOption, item: any) {
        if (btn.style) {
            return btn["style"];
        } else {
            return 'btn-default';
        }
    }

}

