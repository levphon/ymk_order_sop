import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzCardModule } from 'ng-zorro-antd/card';

import { NgsPanel } from './panel';
import {
	NgsPanelConfig, NgsPanelOption, NgsPanelCrumbsOption
} from './panel.config';

export { NgsPanel } from './panel';
export {
	NgsPanelConfig, NgsPanelOption,NgsPanelCrumbsOption
} from './panel.config';

const NGB_TABSET_DIRECTIVES = [NgsPanel];

@NgModule({ 
	declarations: NGB_TABSET_DIRECTIVES, 
	exports: NGB_TABSET_DIRECTIVES, 
	imports: [
		CommonModule,
		NzBreadCrumbModule,
		NzCardModule
	] 
})
export class NgsPanelModule {
	static forRoot(): ModuleWithProviders<any> { return { ngModule: NgsPanelModule, providers: [NgsPanelConfig] }; }
}
