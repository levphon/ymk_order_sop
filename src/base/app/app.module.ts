import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from 'src/module/module-home/home/home.component';
import { LoginComponent } from 'src/module/module-home/login/login.component';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NgsModule } from '../common/ngs';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { HttpService } from '../utlis/http.config';
import { CoreService } from 'src/core/service/core.service';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzSpinModule } from 'ng-zorro-antd/spin';

@NgModule({
  declarations: [
    AppComponent,HomeComponent,LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzLayoutModule,
    NzMenuModule,
    NzAvatarModule,
    NzDropDownModule,
    NgsModule,
    NzMessageModule,
    NzTabsModule,
    NzInputModule,
    FormsModule,
    NzButtonModule,
    NzIconModule,
    NzSpinModule
  ],
  providers: [HttpService,CoreService],
  bootstrap: [AppComponent]
})
export class AppModule { }
