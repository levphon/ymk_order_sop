import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CoreService } from 'src/core/service/core.service';
import { HttpService } from '../utlis/http.config';
import { UserService } from 'src/core/service';
import { NzMessageService } from 'ng-zorro-antd/message';


@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  isCollapsed = false;
  constructor(
    private router: Router,
    private message: NzMessageService,
    private http: HttpService,
    private coreService:CoreService
  ) {

  }
  hasInit = false;
  userService = new UserService(this.http);
  ngOnInit(): void {

    // this.hasInit = this.checkShow();
    let userModel = this.coreService.getUserModel();
    let token = this.coreService.getToken();
    
      if(!token){
          this.hasInit = true;
          this.message.error("会话过期");
          this.router.navigateByUrl(`/login`);
    }else{
      this.userService.userSessionGet().then(res => {
        let data = res.source;

        this.coreService.setToken(data.token);
        this.coreService.setUserModel(data);
        this.hasInit = true;
      });
    }
  }

  checkShow(){
    if(this.coreService.getToken()){
      return true;
    }else{
      return false;
    }
  }
  
}
