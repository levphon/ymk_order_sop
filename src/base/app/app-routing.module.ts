import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsModule } from 'src/module/module-goods';
import { HomeDataComponent } from 'src/module/module-home/home-data/homeData.component';
import { HomeComponent } from 'src/module/module-home/home/home.component';
import { LoginComponent } from 'src/module/module-home/login/login.component';
import { JobModule } from 'src/module/module-job';
import { MchModule } from 'src/module/module-mch';
import { MchUserModule } from 'src/module/module-mchUser';
import { MemberModule } from 'src/module/module-member';
import { OrdersModule } from 'src/module/module-orders';
import { PayPaymentModule } from 'src/module/module-pay';
import { RbacModule } from 'src/module/module-rbac';
import { UserModule } from 'src/module/module-user';



const routes: Routes = [
    { path: 'login', component: LoginComponent },
    {
      path: '', component: HomeComponent,
      children: [
        { path: '', component: HomeDataComponent },
        { path: '',loadChildren: () => UserModule },
        { path: '',loadChildren: () => RbacModule },
        { path: '',loadChildren: () => MchModule },
        { path: '',loadChildren: () => MchUserModule },
        { path: '',loadChildren: () => MemberModule },
        { path: '',loadChildren: () => GoodsModule },
        { path: '',loadChildren: () => OrdersModule },
        { path: '',loadChildren: () => PayPaymentModule },
        { path: '',loadChildren: () => JobModule }
      ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
