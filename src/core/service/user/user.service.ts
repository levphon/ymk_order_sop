import { HttpService } from "src/base/utlis/http.config"
import { CmdSignModel } from "src/core/module/CmdSignModel";
import { SessionCmdCode } from "src/module/module-home/core";

export class UserService {

    constructor(private http :HttpService) {
    }

    public userSessionPost(data:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = SessionCmdCode.SOP_MEMBER_POST;
        cmdSign.source = data;
        return this.http.exe(cmdSign);
    }

    public userSessionGet(reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = SessionCmdCode.SOP_MEMBER_GET;
        return this.http.exe(cmdSign);
    }
    
}