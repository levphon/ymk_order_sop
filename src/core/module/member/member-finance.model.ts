import { BaseModel } from "../base.model";
import { MemberModel } from "./member.model";


export class MemberFinanceModel extends BaseModel {

    /**
      * id
      */
    memberId!: number;

    /**
     * 可提现余额
     */
    transferAmount!: number;

    /**
     * 可预约的时长
     */
    transferDuration!: number;

    freezeAmount!: number;

    memberBO!: MemberModel;

    payDivideCount!: number;
}