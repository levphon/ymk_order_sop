import { BaseModel } from "../base.model";
import { MchModel } from "../mch/mch.model";


export class MemberModel extends BaseModel {

    /**
     * 名称
     */
    name!: string;

    /**
     * 手机号
     */
    mobile!: string;

    /**
     * 头像
     */
    avatarUrl!: string;

    /**
     * 状态 0禁用 1正常
     */
    status!: number;

    mchBO!:MchModel;
}