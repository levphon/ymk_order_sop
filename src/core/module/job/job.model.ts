import { BaseModel } from "../base.model";


export class JobModel extends BaseModel {

    /**
     * job goup，与job name唯一标示
     */
    jobGroup!: string;

    /**
     * job名字，与java name一致
     */
    jobName!: string;

    /**
     * job编码，与spring name一致
     */
    jobCode!: string;

    /**
     * job中文说明
     */
    jobDesc!: string;

    /**
     * 1正常0停用
     */
    status!: number;

    /**
     * 上次执行完成时间
     */
    lastExeDate!: Date;

    /**
     * cron表达式
     */
    cronExpression!: string;

}