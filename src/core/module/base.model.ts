
export abstract class BaseModel {
    id!: number;
    deleted!: number;
    createdDate!: Date;
    modifiedDate!: Date;
    createdBy!: number;
    modifiedBy!: number;
}
