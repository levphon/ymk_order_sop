import { BaseModel } from "../base.model";
import { RbacPermModel } from "../rbac/rbac-perm.model";
import { RbacRoleModel } from "../rbac/rbac-role.model";
import { RbacUserRoleModel } from "../rbac/rbac-user-role.model";


export class MchUserModel extends BaseModel {

    /**
     * 联系人名称
     */
    name!: string;

    /**
     * 手机号
     */
    mobile!: string;

    /**
     * 状态
     */
    status!: string;

    rbacUserRoleBO!:RbacUserRoleModel;
}