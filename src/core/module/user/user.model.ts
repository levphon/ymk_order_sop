import { BaseModel } from "../base.model";
import { RbacPermModel } from "../rbac/rbac-perm.model";
import { RbacRoleModel } from "../rbac/rbac-role.model";
import { RbacUserRoleModel } from "../rbac/rbac-user-role.model";


export class UserModel extends BaseModel {

    name!: string;
    mobile!: string;
    password!: string;
    avatarUrl!: string;
    status!: number;
    token!: string;
    roleId!:number;
    rbacRoleBO!:RbacRoleModel;
    rbacUserRoleBO!:RbacUserRoleModel;
    rbacPermList!: Array<RbacPermModel>;
}