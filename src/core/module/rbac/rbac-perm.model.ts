import { BaseModel } from "../base.model";


export class RbacPermModel extends BaseModel {

    /**
     * 1=menu 2=page 3=op
     */
    type!: number;
    /**
     * 功能名称
     */
    name!: string;

    code!: string;
    /**
     * 功能url
     */
    icon!: string;
    /**
     * 菜单id
     */
    parentId!: number;
    /**
     * 排序号
     */
    sort!: number;
    /**
     * 是否sop
     */
    isSop!: number;
    
    children!: Array<RbacPermModel>;

    style:any;

    check!:boolean;
}