
import { BaseModel } from "../base.model";
import { RbacPermModel } from "./rbac-perm.model";
import { RbacRolePermModel } from "./rbac-role-perm.model";


export class RbacRoleModel extends BaseModel {

    /**
     * 角色名称
     */
    /**
     * 角色名称
     */
    name!: string;
    /**
     * 1正常2停用
     */
    /**
     * 1正常2停用
     */
    status!: number;
    /**
     * 权限id list
     */
    permIdList!:Array<number>;

    rbacPermBOList!:Array<RbacPermModel>;

    rbacRolePermVOList!: Array<RbacRolePermModel>;
}