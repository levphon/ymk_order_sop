import { BaseModel } from "../base.model";
import { MemberModel } from "../member/member.model";
import { PayPaymentModel } from "../pay/payPayment.model";
import { OrdersItemModel } from "./orders-item.model";


export class OrdersModel extends BaseModel {


    /**
     * 订单id
     */
    orderNo!: string;

    /**
     * id
     */
    memberId!: number;

    /**
     * 上级id
     */
    superiorId!: number;

    /**
     * 商品名称
     */
    goodsTitle!: string;

    /**
     * 订单类型 1成品订单 2定制订单 3服务订单
     */
    type!: number;

    /**
     * 金额
     */
    amount!: number;

    /**
     * 分账金额
     */
    superiorAmount!: number;

    /**
     * 盈利金额
     */
    profitAmount!: number;

    /**
     * 支付类型 1支付宝 2微信
     */
    payType!: number;

    /**
     * 状态 1 已完成 2待支付 3已关闭
     */
    status!: number;

    orderMemberBO!: MemberModel;

    payPaymentBO!: PayPaymentModel;

    ordersItemBOList!:Array<OrdersItemModel>;

}

