import { BaseModel } from "../base.model";
import { GoodsModel } from "../goods/goods.model";
import { MemberModel } from "../member/member.model";
import { PayPaymentModel } from "../pay/payPayment.model";


export class OrdersItemModel extends BaseModel {


    /**
     * 订单id
     */
     ordersId!: number;

    /**
     * id
     */
    memberId!: number;

    /**
     * 上级id
     */
     goodsId!: number;


    /**
     * 金额
     */
    amount!: number;

   

    goodsBO!: GoodsModel;



}

