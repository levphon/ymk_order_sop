import { BaseModel } from "../base.model";


export class GoodsCustomizationModel extends BaseModel {

    /**
     * 商品id
     */
    goodsId!: number;

    /**
     * 是否需要文档
     */
    isDocument!: number;

    /**
     * 是否需要讲解
     */
    isExplain!: number;

    /**
     * 项目描述
     */
    desc!: string;

    /**
     * 联系方式 1微信 2qq
     */
    contactType!: number;

    /**
     * 联系方式
     */
    contactData!: string;

    /**
     * 交付时间
     */
    deliveryDate!: Date;
}