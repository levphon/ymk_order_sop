import { BaseModel } from "../base.model";


export class GoodsExtModel extends BaseModel {

    /**
     * 商品id
     */
    goodsId!: number;

    /**
     * 演示图片
     */
    imageList!: string;

    /**
     * 文档
     */
    content!: string;

    /**
     * 文档目录
     */
     contentMenus!: string;

    /**
     * 视频
     */
    video!: string;

    /**
     * 下载链接
     */
    downloadUrl!: string;

    /**
     * 版本
     */
    version!: number;
}