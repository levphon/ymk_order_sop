import { BaseModel } from "../base.model";
import { MchModel } from "../mch/mch.model";


export class GoodsCatPropModel extends BaseModel {

    /**
     * 商户id
     */
    mchId?: number;

    /**
     * 商品id
     */
    goodsId?: number;

    /**
     * 标签名称
     */
    name!: string;

    /**
     * 排序
     */
    sort!: number;

    /**
     * 状态
     */
    orderSort!: number;

    style?:any;
    edit?:boolean = false;

}