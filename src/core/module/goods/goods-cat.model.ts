import { BaseModel } from "../base.model";
import { MchModel } from "../mch/mch.model";


export class GoodsCatModel extends BaseModel {

    /**
     * 商户id
     */
    mchId!:number;

    /**
     * 标签名称
     */
    name!:string;

    /**
     * 封面
     */
    cover!:string;

    /**
     * 排序
     */
    sort!:number;

    /**
     * 状态
     */
    status!:string;

    mchBO!:MchModel
}