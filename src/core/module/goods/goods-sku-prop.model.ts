import { BaseModel } from "../base.model";
import { MchModel } from "../mch/mch.model";


export class GoodsSkuPropModel extends BaseModel {

    /**
     * 商户id
     */
    mchId!: number;

    /**
     * 商品id
     */
    goodsId!: number;

    /**
     * 标签名称
     */
    name!: string;

}