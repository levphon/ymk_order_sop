import { BaseModel } from "../base.model";
import { MchModel } from "../mch/mch.model";
import { GoodsCatModel } from "./goods-cat.model";
import { GoodsCustomizationModel } from "./goods-customization.model";
import { GoodsExtModel } from "./goods-ext.model";


export class GoodsModel extends BaseModel {

  /**
     * 商户id
     */
  mchId!: number;

  /**
   * 名称
   */
  title!: string;

  /**
   * 子标题
   */
  subTitle!: string;

  /**
   * 商品类型
   */
  catId!: string;

  /**
   * 封面图片
   */
  coverUrl!: string;

  /**
   * 描述
   */
  desc!: string;

  /**
   * 成本价
   */
  costPrice!: number;

  /**
   * 价格
   */
  price!: number;

  /**
   * 销售价格
   */
  salePrice!: number;

  /**
   * 销售数量
   */
  salesVolume!: number;

  /**
   * 商品备注
   */
  remark!: string;

  /**
   * 热度
   */
  heat!: number;

  /**
   * 版本
   */
  version!: number;

  /**
   * 状态
   */
  status!: string;

  goodsCatBO!: GoodsCatModel;
  mchBO!:MchModel;
}