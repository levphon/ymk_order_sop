export class CmdSignModel {
	scope: string = 'admin';
	code!:number;
	cmdCode:string|any;
	reqCode:string|any;
	respCode:string|any;
	cmdVersion:string|any=1;
	msg?: string;
	token:string|any;
	source: any;
	returnStruct?:string;
	userId?: number;
	pageSize?:number=10;
	pageIndex?: number=1;
	pageTotal?: number=10;

}

export class CmdData {
	cmdCode:string|any;
	reqCode?:string|any;
	source?: any = {};
	returnStruct?:string;
	userId?: number;
	pageSize?:number=10;
	pageIndex?: number=1;
	pageTotal?: number=10;
}