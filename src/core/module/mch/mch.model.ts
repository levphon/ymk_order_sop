import { BaseModel } from "../base.model";
import { MchUserModel } from "../user/mchUser.model";


export class MchModel extends BaseModel {

    /**
     * 联系人名称
     */
    mchUserId!: number;

    /**
     * 商户logo
     */
    logo!: string;

    /**
     * 商户名称
     */
    title!: string;

    /**
     * 分账比例
     */
    divideRatio!: number;

    /**
     * 销售总额
     */
    salesAmount!: number;

    /**
     * 分账总额
     */
    divideAmount!: number;

    /**
     * 待分账金额
     */
    stayDivideAmount!: number;

    /**
     * 状态
     */
    status!: string;

    mchUserBO!: MchUserModel;
}