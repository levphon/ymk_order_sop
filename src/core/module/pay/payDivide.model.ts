import { BaseModel } from "../base.model";
import { MemberModel } from "../member/member.model";
import { OrdersModel } from "../orders/orders.model";
import { PayPaymentModel } from "./payPayment.model";


export class PayDivideModel extends BaseModel {


    /**
     * id
     */
    memberId!: number;

    /**
     * 上级id
     */
     superiorId!: number;

    /**
     * 订单id
     */
    ordersId!: number;

    /**
    * 分账id
    */
     payPaymentId!: number;


    /**
     * 金额
     */
    amount!: number;

    /**
     * 订单编号
     */
    tradeNo !: string;

    /**
     * 状态 1 已完成 2待支付 3已关闭
     */
    status!: number;

    orderMemberBO!: MemberModel;
    ordersBO!: OrdersModel;
    memberBO!: MemberModel;
    payPaymentBO!: PayPaymentModel;
}

