import { BaseModel } from "../base.model";
import { MemberModel } from "../member/member.model";
import { OrdersModel } from "../orders/orders.model";


export class PayPaymentModel extends BaseModel {


    /**
     * 订单id
     */
    payNo!: string;

    /**
     * id
     */
    memberId!: number;



    /**
     * 订单id
     */
    ordersId!: number;

    /**
    * 分账id
    */
    divideId!: number;


    /**
     * 金额
     */
    amount!: number;

    /**
     * 分账金额
     */
    superiorAmount!: number;

    /**
     * 盈利金额
     */
    profitAmount!: number;

    /**
     * 订单编号
     */
    tradeNo !: string;

    /**
     * 支付类型 1支付宝 2微信
     */
    payType!: number;

    /**
     * 状态 1 已完成 2待支付 3已关闭
     */
    status!: number;

    orderMemberBO!: MemberModel;
    ordersBO!: OrdersModel;

}

