import { BaseModel } from "../base.model";
import { MemberModel } from "../member/member.model";


export class PayWithdrawModel extends BaseModel {

    /**
     * 会员id
     */
    memberId!: number;

    /**
     * 提现金额
     */
    withdrawAmount!: number;

    /**
     * 0已关闭 1已完成 2待处理
     */
    status!: number;

    /**
     * 备注
     */
    desc!: string;

    memberBO!:MemberModel;

}

