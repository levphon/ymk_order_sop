export class ReqCode {
    /**
     * rbac
     */
    public static readonly UPDATE_STATUS = "UPDATE_STATUS";
    /**
     * 查看全部
     */
    public static readonly LIST_ALL = "LIST_ALL";
    /**
     * 同步es
     */
    public static readonly ES_GOODS_SYNC = "ES_GOODS_SYNC";
    /**
     * 同步es
     */
    public static readonly PAY_DIVIDE_PATCH = "PAY_DIVIDE_PATCH";
    /**
     * 修改订单金额
     */
    public static readonly PATCH_ORDERS_AMOUNT = "PATCH_ORDERS_AMOUNT";
    /**
     * 立即执行一次job
     */
    public static readonly IMMEDIATE_EXECUTION_JOB = "IMMEDIATE_EXECUTION_JOB";
    /**
     * 修改热度
     */
    public static readonly UPDATE_HEAT = "UPDATE_HEAT";

}