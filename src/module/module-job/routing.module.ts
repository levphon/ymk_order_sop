import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JobEditComponent } from './page/edit/edit.component';
import { JobListComponent } from './page/list/list.component';

const routes: Routes = [
  { path: 'job/list', component: JobListComponent },
  { path: 'job/post', component: JobEditComponent },
  { path: 'job/:id/edit', component: JobEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobRoutingModule { }
