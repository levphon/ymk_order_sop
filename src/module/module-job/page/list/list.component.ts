import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { HttpService } from 'src/base/utlis/http.config';
import { ReqCode } from 'src/core/code';
import { CmdData } from 'src/core/module/CmdSignModel';
import { JobModel } from 'src/core/module/job/job.model';
import { JobCmdCode } from '../../core';
import { JobService } from '../../core/service/JobService';


@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class JobListComponent implements OnInit {

  constructor(
    private message:NzMessageService,
    private router: Router,
    private http:HttpService,
    private modal: NzModalService
  ) { }

  jobService:JobService = new JobService(this.http);
  @ViewChild('myForm') myForm: NgsForm | any;
  @ViewChild('myTable') myTable: NgsTable | any;
  @ViewChild('myPostForm') myPostForm: NgsForm | any;

  panelOption:NgsPanelOption={
    crumbs:[
      {
        text:'任务列表'
      }
    ],
    buttons:[
      {
        text:'添加',
        action:() => {
            this.jobModal.isVisible = true;
        }
      }
    ]
  }

  formOption:NgsFormOption = {
    showSearch:true,
    column:4,
    components:[
      { label:'任务名称', property:'jobName' ,comp:'NgsFormInput', }
    ]
  }

  cmdSign:CmdData={
    cmdCode:JobCmdCode.JOB_LIST
  };

  statusTemplate:Array<NgsTabColumnBadgeOption> = [
    { status:0,tagLabel:'禁用',tagColor:'red' },
    { status:1,tagLabel:'正常',tagColor:'blue' }
  ]
  
  tableOption:NgsTableOption = {
    dataSource:this.cmdSign,
    selections:[
      { text:'全部',value:null },
      { text:'启用',value:1 },
      { text:'禁用',value:0 }
    ],
    table:{
      columns:[
        { title:'任务名称',property:'jobName' },
        { title:'任务分组',property:'jobGroup' },
        { title:'任务code',property:'jobCode' },
        { title:'状态',property:'status' ,badgeOption:this.statusTemplate},
        { title:'任务描述',property:'jobDesc' },
        { title:'cron',property:'cronExpression' },
        { title:'创建时间',property:'createdDate' }
      ],
      op:{
        buttons:[
          {
            text:"立即执行",
            action:(item:JobModel) => {
              let jobModel:JobModel = new JobModel();
              jobModel.id = item.id;
              jobModel.jobCode = item.jobCode;
              jobModel.jobGroup = item.jobGroup;
              this.jobService.jobPost(jobModel,ReqCode.IMMEDIATE_EXECUTION_JOB).then(res => {
                this.message.success("执行完成！");
              })
            }
          },
          {
            text:"编辑",
            action:(item:JobModel) => {
              let jobModel:JobModel = new JobModel();
              jobModel.id = item.id;
              this.jobService.jobGet(jobModel).then(res => {
                this.jobModal.isVisible = true;
                this.jobModal.isUpdate = true;
                this.jobModal.jobId = item.id;
                setTimeout(() => {
                  this.myPostForm.setValue(res.source);
                }, 100);
                
              });
            }
          },
          {
            text:(item:JobModel) => {
              return item.status == 1?"禁用":"启用";
            },
            action:(item:JobModel) => {
              let jobModel:JobModel = new JobModel();
              jobModel.id = item.id;
              jobModel.status = item.status === 1?0:1;
              this.jobService.jobPatch(jobModel,ReqCode.UPDATE_STATUS).then(res => {
                this.message.success("修改完成");
                this.myTable.search();
              });
            }
          },
          {
            text:'删除',
            action:(item:any) => {
              this.modal.create({
                nzTitle: '是否删除该用户',
                nzContent: '删除后无法恢复！',
                nzClosable: false,
                nzOnOk: () => {
                  this.jobService.jobDel(item).then(res => {
                    this.message.success("删除完成");
                    this.myTable.search();
                  });
                }
              });
            }
          },
        ]
      }
    }
  }

  ngOnInit() {
  }

  onSearch(event:any){
    this.myTable.search(event);
  }

  formPostOption:NgsFormOption = {
    showSearch:false,
    column:1,
    components:[
      { label:'任务名称', property:'jobName' ,comp:'NgsFormInput',require:true },
      { label:'任务code', property:'jobCode' ,comp:'NgsFormInput',require:true },
      { label:'任务分组', property:'jobGroup' ,comp:'NgsFormInput',require:true },
      { label:'任务cron', property:'cronExpression' ,comp:'NgsFormInput',require:true },
      { label:'任务描述', property:'jobDesc' ,comp:'NgsFormInput',require:true },
    ]

  }

  jobModal = {
    isVisible: false,
    isUpdate: false,
    jobId: 0
  }
  

  jobPost(){
    let myForm = this.myPostForm.getValue();
    if(this.jobModal.isUpdate){

      myForm.id = this.jobModal.jobId;
      this.jobService.jobPatch(myForm).then(res => {
        this.message.success("编辑成功!");
        this.jobModal.isVisible = false;
        this.jobModal.isUpdate = false;
        this.jobModal.jobId = 0;
        this.myTable.search();
      })
    }else{
      this.jobService.jobPost(myForm).then(res => {
        this.message.success("新建成功!");
        this.jobModal.isVisible = false;
        this.myTable.search();
      })
    }
    
  }

}
