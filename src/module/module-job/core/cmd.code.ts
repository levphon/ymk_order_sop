export class JobCmdCode {
    public static readonly JOB_GET = "JOB_GET";
    public static readonly JOB_LIST = "JOB_LIST";
    public static readonly JOB_POST = "JOB_POST";
    public static readonly JOB_PATCH = "JOB_PATCH";
    public static readonly JOB_DEL = "JOB_DEL";

}