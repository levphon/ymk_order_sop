import { HttpService } from "src/base/utlis/http.config"
import { CmdSignModel } from "src/core/module/CmdSignModel";
import { JobCmdCode } from "../cmd.code";

export class JobService {

    constructor(private http :HttpService) {
    }

    public jobGet(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = JobCmdCode.JOB_GET;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public jobList(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = JobCmdCode.JOB_LIST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public jobPatch(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = JobCmdCode.JOB_PATCH;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public jobPost(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = JobCmdCode.JOB_POST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public jobDel(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = JobCmdCode.JOB_DEL;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }
    
}