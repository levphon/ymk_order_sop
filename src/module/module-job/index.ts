import { NgModule } from '@angular/core';
import { NgsModule } from 'src/base/common/ngs';

import { JobRoutingModule } from './routing.module';

import { CommonModule } from '@angular/common';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';
import { JobEditComponent } from './page/edit/edit.component';
import { JobListComponent } from './page/list/list.component';


const DIRECTIVES = [JobEditComponent, JobListComponent]

@NgModule({
  declarations: DIRECTIVES,
  exports: DIRECTIVES,
  imports:
    [
      JobRoutingModule,
      NgsModule,
      CommonModule,
      NzEmptyModule,
      NzModalModule,
      NzCheckboxModule,
      FormsModule
    ],
  entryComponents: DIRECTIVES,
  providers: []
})
export class JobModule { }
