export class MchCmdCode {
    public static readonly MCH_LIST = "MCH_LIST";
    public static readonly MCH_POST = "MCH_POST";
    public static readonly MCH_DEL = "MCH_DEL";
    public static readonly MCH_GET = "MCH_GET";
    public static readonly MCH_PATCH = "MCH_PATCH";

}