import { HttpService } from "src/base/utlis/http.config"
import { CmdSignModel } from "src/core/module/CmdSignModel";
import { MchCmdCode } from "../cmd.code";

export class MchService {

    constructor(private http: HttpService) { }

    public mchList(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchCmdCode.MCH_LIST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public mchGet(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchCmdCode.MCH_GET;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public mchPatch(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchCmdCode.MCH_PATCH;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public mchPost(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchCmdCode.MCH_POST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public mchDel(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchCmdCode.MCH_DEL;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

}