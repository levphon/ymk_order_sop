import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { HttpService } from 'src/base/utlis/http.config';
import { ReqCode } from 'src/core/code';
import { CmdData } from 'src/core/module/CmdSignModel';
import { MchUserCmdCode } from 'src/module/module-mchUser/code/cmd.code';
import { MemberCmdCode } from 'src/module/module-member/core/cmd.code';
import { MchService } from '../../code/service/MchService';

@Component({
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class MchEditComponent implements OnInit {

    constructor(
        private message: NzMessageService,
        private router: Router,
        private http: HttpService,
        private route: ActivatedRoute,
    ) { }

    mchService: MchService = new MchService(this.http);

    panelOption: NgsPanelOption = {
        crumbs: [
            {
                text: '商户列表',
                action: () => {
                    this.router.navigateByUrl(`/mch/list`)
                }
            },
            {
                text: '商户操作'
            }
        ],
        buttons: [
            {
                text: '保存',
                action: () => {
                    let myForm = this.myForm.getValue();

                    if (this.bizId) {
                        myForm.id = this.bizId;
                        this.mchService.mchPatch(myForm).then(res => {
                            this.router.navigateByUrl("/mch/list");
                        });
                    } else {
                        this.mchService.mchPost(myForm).then(res => {
                            this.router.navigateByUrl("/mch/list");
                        });
                    }

                }
            }
        ]
    }

    @ViewChild('myForm') myForm: NgsForm | any;

    memberGetCmd:CmdData = {
        cmdCode:MchUserCmdCode.MCH_USER_LIST,
        source:{status:'NORMAL'}
    }
    formOption: NgsFormOption = {
        showSearch: false,
        column: 3,
        components: [
            { label: '商户名称', property: 'title', comp: 'NgsFormInput', require: true, placeHolder: '请输入商户名称' },
            { label: '联系人', property: 'mchUserId', comp:'NgsFormSelect',
            dataSource:this.memberGetCmd,dsLabel:'name',dsValue:"id",
            openSerch:true,dsSubValue:'mobile',
            require: true, placeHolder: '请选择联系人' },
            { label: '分账比例', property: 'divideRatio', comp:'NgsFormInput',type:'number', require: true, placeHolder: '请输入分账比例' },
            { label: '商户logo', property: 'logo', comp:'NgsFormUploader',data:[],accept:'image',limit:1 },
        ]
    }

    bizId: number | any;
    ngOnInit() {
        this.route.params.subscribe((params: any) => {
            if (params.id) {
                this.bizId = params.id;

                let model: any = {};
                model.id = params.id;
                this.mchService.mchGet(model).then(res => {
                    let data = res.source;
                    this.myForm.setValue(data);
                })
            }
        })
    }

}
