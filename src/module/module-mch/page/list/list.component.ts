import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CmdData, CmdSignModel } from 'src/core/module/CmdSignModel';
import { HttpService } from 'src/base/utlis/http.config';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NzMessageService } from 'ng-zorro-antd/message';
import { MchModel } from 'src/core/module/mch/mch.model';
import { MchService } from '../../code/service/MchService';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { NzModalService } from 'ng-zorro-antd/modal';
import { MchCmdCode } from '../../code/cmd.code';
import { MchUserCmdCode } from 'src/module/module-mchUser/code/cmd.code';

@Component({
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class MchListComponent implements OnInit {

    constructor(
        protected http: HttpService,
        private router: Router,
        private message: NzMessageService,
        private modal: NzModalService
    ) { }

    mchService: MchService = new MchService(this.http);
    @ViewChild('myForm') myForm: NgsForm | any;
    @ViewChild('myTable') myTable: NgsTable | any;

    panelOption: NgsPanelOption = {
        crumbs: [
            {
                text: "商户列表",
            }],
        buttons: [
            {
                text: '新增',
                action: () => {
                    this.router.navigateByUrl(`/mch/post`)
                }
            }
        ]
    }



    mchGetCmd:CmdData = {
        cmdCode:MchUserCmdCode.MCH_USER_LIST
    }
    formOption: NgsFormOption = {
        column: 4,
        showSearch: true,
        components: [
            { label: '商户名称', property: 'title', placeHolder: '商户名称', comp: 'NgsFormInput' },
            { label: '联系人', property: 'mchUserId', comp:'NgsFormSelect',dataSource:this.mchGetCmd,dsLabel:'name',dsValue:"id",
                openSerch:true,dsSubValue:'mobile',require: true, placeHolder: '请选择联系人' },
        ]
    }

    statusTemplate: Array<NgsTabColumnBadgeOption> = [
        { status: 'DISABLED', tagLabel: '已禁用', tagColor: 'red' },
        { status: 'NORMAL', tagLabel: '正常', tagColor: 'green' },
        { status: 'HAVE_EXPIRED', tagLabel: '已过期', tagColor: '#999' },
    ]

    cmdSign: CmdData = {
        cmdCode: MchCmdCode.MCH_LIST
    };

    tableOption: NgsTableOption = {
        selections: [
            { text: '全部', value: null },
            { text: '已禁用', value: 'DISABLED' },
            { text: '正常', value: 'NORMAL' },
            { text: '已过期', value: 'HAVE_EXPIRED' },
        ],
        dataSource: this.cmdSign,
        table: {
            columns: [
                {
                    title: '联系人',
                    property: 'user',
                    info:(item:MchModel) => {
                        return item.mchUserBO.name;
                    },
                    subOption:(item:MchModel) => {
                        return item.mchUserBO.mobile;
                    }
                },
                {
                    title: '商户名称',
                    property: 'title'
                },
                {
                    title: '分账比例',
                    property: 'divideRatio'
                },
                {
                    title: '销售总额',
                    property: 'salesAmount'
                },
                {
                    title: '分账总额',
                    property: 'divideAmount'
                },
                {
                    title: '待分账金额',
                    property: 'stayDivideAmount'
                },
                {
                    title: '状态',
                    property: 'status',
                    badgeOption: this.statusTemplate,
                    width: 180
                },
                {
                    title: '创建日期',
                    property: 'createdDate'
                },
            ],
            op: {
                width: '200px',
                buttons: [
                    {
                        text: '编辑',
                        action: (item: any) => {
                            this.router.navigateByUrl('mch/' + item.id + '/edit');
                        }
                    },
                    {
                        text: (item: any) => {
                            return item.status == 0 ? '启用' : '禁用';
                        },
                        action: (item: any) => {
                            let model: any = {};
                            model.id = item.id;
                            model.status = item.status == 0 ? 1 : 0;
                            this.mchService.mchPatch(model).then(res => {
                                this.message.success("修改完成");
                                this.myTable.search();
                            });
                        }
                    },
                    {
                        text: '删除',
                        action: (item: any) => {
                            this.modal.create({
                                nzTitle: '是否删除该',
                                nzContent: '删除后无法恢复！',
                                nzClosable: false,
                                nzOnOk: () => {
                                    this.mchService.mchDel(item).then(res => {
                                        this.message.success("删除完成");
                                        this.myTable.search();
                                    });
                                }
                            });
                        }
                    },
                ]
            }
        }
    }

    ngOnInit() { }

    onSearch(event: any) {
        this.myTable.search(event);
    }
}