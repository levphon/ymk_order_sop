import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MchListComponent } from './page/list/list.component';
import { MchEditComponent } from './page/edit/edit.component';

const routes: Routes = [
    { path: 'mch/list', component: MchListComponent },
    { path: 'mch/post', component: MchEditComponent },
    { path: 'mch/:id/edit', component: MchEditComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MchRoutingModule { }
