import { NgModule } from '@angular/core';
import { NgsModule } from 'src/base/common/ngs';

import { MchEditComponent } from './page/edit/edit.component';
import { MchListComponent } from './page/list/list.component';
import { MchRoutingModule } from './routing.module';

import { CommonModule } from '@angular/common';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';


const DIRECTIVES = [MchEditComponent, MchListComponent]

@NgModule({
    declarations: DIRECTIVES,
    exports: DIRECTIVES,
    imports:
        [
            MchRoutingModule,
            NgsModule,
            CommonModule,
            NzEmptyModule,
            NzModalModule,
            NzCheckboxModule,
            FormsModule
        ],
    entryComponents: DIRECTIVES,
    providers: []
})
export class MchModule { }
