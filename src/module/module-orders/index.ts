import { NgModule } from '@angular/core';
import { NgsModule } from 'src/base/common/ngs';


import { OrdersRoutingModule } from './routing.module';
import { NzTabsModule } from 'ng-zorro-antd/tabs';

import { CommonModule } from '@angular/common';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';
import { OrdersListComponent } from './page/list/list.component';
import { OrdersDetailsComponent } from './page/details/details.component';
import { OrdersComponent } from './page/details/common/orders.component';
import { NzTableModule } from 'ng-zorro-antd/table';


const DIRECTIVES = [OrdersListComponent,OrdersDetailsComponent,OrdersComponent]

@NgModule({
  declarations: DIRECTIVES,
  exports: DIRECTIVES,
  imports:
    [
      OrdersRoutingModule,
      NgsModule,
      CommonModule,
      NzEmptyModule,
      NzModalModule,
      NzCheckboxModule,
      FormsModule,
      NzTabsModule,
      NzTableModule,
      NzInputNumberModule
    ],
  entryComponents: DIRECTIVES,
  providers: []
})
export class OrdersModule { }
