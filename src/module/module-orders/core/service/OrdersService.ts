import { HttpService } from "src/base/utlis/http.config"
import { CmdSignModel } from "src/core/module/CmdSignModel";
import { OrdersCmdCode } from "../cmd.code";

export class OrdersService {

    constructor(private http :HttpService) {
    }

    public ordersGet(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = OrdersCmdCode.ORDERS_GET;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public ordersList(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = OrdersCmdCode.ORDERS_LIST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public ordersPatch(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = OrdersCmdCode.ORDERS_PATCH;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public ordersPost(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = OrdersCmdCode.ORDERS_POST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public ordersDel(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = OrdersCmdCode.ORDERS_DEL;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }
    
}