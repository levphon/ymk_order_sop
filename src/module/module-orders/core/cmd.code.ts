export class OrdersCmdCode {
  
    public static readonly ORDERS_GET = "ORDERS_GET";
    public static readonly ORDERS_LIST = "ORDERS_LIST";
    public static readonly ORDERS_POST = "ORDERS_POST";
    public static readonly ORDERS_PATCH = "ORDERS_PATCH";
    public static readonly ORDERS_DEL = "ORDERS_DEL";

    public static readonly ORDERS_ITEM_GET = "ORDERS_ITEM_GET";
    public static readonly ORDERS_ITEM_LIST = "ORDERS_ITEM_LIST";
    public static readonly ORDERS_ITEM_POST = "ORDERS_ITEM_POST";
    public static readonly ORDERS_ITEM_PATCH = "ORDERS_ITEM_PATCH";
    public static readonly ORDERS_ITEM_DEL = "ORDERS_ITEM_DEL";

    public static readonly ORDERS_SUBMIT_CHAIN_GET = "ORDERS_SUBMIT_CHAIN_GET";
    public static readonly ORDERS_SUBMIT_CHAIN_LIST = "ORDERS_SUBMIT_CHAIN_LIST";
    public static readonly ORDERS_SUBMIT_CHAIN_POST = "ORDERS_SUBMIT_CHAIN_POST";
    public static readonly ORDERS_SUBMIT_CHAIN_PATCH = "ORDERS_SUBMIT_CHAIN_PATCH";
    public static readonly ORDERS_SUBMIT_CHAIN_DEL = "ORDERS_SUBMIT_CHAIN_DEL";

}