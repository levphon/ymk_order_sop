import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersDetailsComponent } from './page/details/details.component';
import { OrdersListComponent } from './page/list/list.component';


const routes: Routes = [
  { path: "orders/list", component: OrdersListComponent },
  { path: "orders/details/:id", component: OrdersDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
