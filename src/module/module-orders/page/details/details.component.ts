import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgsPanelOption } from 'src/base/common/ngs';
import { HttpService } from 'src/base/utlis/http.config';
import { OrdersService } from '../../core/service/OrdersService';


@Component({
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class OrdersDetailsComponent implements OnInit {

  constructor(
    private router: Router,
    private http: HttpService,
    private route: ActivatedRoute
  ) { }

  ordersService: OrdersService = new OrdersService(this.http);
  

  panelOption: NgsPanelOption = {
    crumbs: [
      {
        text: '订单列表',
        action: () => {
          this.router.navigateByUrl(`/orders/list`)
        }
      },
      {
        text: '订单详情'
      }
    ]
  }
 
  ordersId: number = 0;

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      if (params.id) {
        this.ordersId = params.id;
      }
    })
  }


}
