import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgsDescOption, NgsPanelOption } from 'src/base/common/ngs';
import { HttpService } from 'src/base/utlis/http.config';
import { GoodsModel } from 'src/core/module/goods/goods.model';
import { OrdersItemModel } from 'src/core/module/orders/orders-item.model';
import { OrdersModel } from 'src/core/module/orders/orders.model';
import { PayPaymentModel } from 'src/core/module/pay/payPayment.model';
import { OrdersService } from '../../../core/service/OrdersService';


@Component({
    exportAs: 'ngs-orders',
    selector: 'ngs-orders',
    templateUrl: './orders.component.html',
    styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

    constructor(
        private router: Router,
        private http: HttpService
    ) { }
    

    ordersService: OrdersService = new OrdersService(this.http);
    @Input() ordersId: number | any;
    orderModel: OrdersModel = new OrdersModel();
    isCustom:boolean = false;
    customDesc:string = "";

    panelOption: NgsPanelOption = {
        crumbs: [
            {
                text: '订单列表',
                action: () => {
                    this.router.navigateByUrl(`/orders/list`)
                }
            },
            {
                text: '订单详情'
            }
        ]
    }

    baseOption: NgsDescOption = {
        dataSource: this.orderModel,
        descItemOption: [
            {
                key: '订单编号',
                value: 'orderNo'
            },
            {
                key: '订单类型',
                info: (item: OrdersModel) => {
                    if (item.type === 2) {
                        return "服务订单";
                    }
                    if (item.type === 3) {
                        return "定制订单";
                    }
                    return "常规订单";
                },
            },
            {
                key: '创建时间',
                value: 'createdDate'
            },
            {
                key: '订单金额',
                value: 'amount'
            },
            {
                key: '支付类型',
                info:(item:OrdersModel) => {
                    return item.payType == 1?"支付宝支付":"微信支付";
                }
            }
        ]
    }

    orderMemberOption: NgsDescOption = {
        dataSource: this.orderModel.orderMemberBO,
        descItemOption: [
            {
                key: '姓名',
                value: 'name'
            }, 
            {
                key: '手机号码',
                value: 'mobile'
            },
            {
                key: '用户类型',
                info: (item: OrdersModel) => {
                    return "普通用户";
                },
            }
        ]
    }

    payPaymentOption: NgsDescOption = {
        dataSource: this.orderModel.payPaymentBO,
        descItemOption: [
            {
                key: '支付单号',
                value: 'payNo'
            }, 
            {
                key: '支付代理',
                info: (item:PayPaymentModel) => {
                    return item.orderMemberBO.name;
                }
            },
            {
                key: '支付金额',
                info: (item: PayPaymentModel) => {
                    return item.amount+"";
                },
            },
            {
                key: '回执编号',
                value:'tradeNo'
            },
            {
                key: '下单时间',
                value:'createdDate'
            },
            {
                key: '支付时间',
                value:'payDate'
            }
        ]
    }

    ordersGoodsOption: NgsDescOption = {
        dataSource: this.orderModel,
        descItemOption: [
            {
                key: '商品名称',
                value: 'title'
            },
            {
                key: '预计交付时间',
                value: 'deliveryDate'
            }
        ]
    }




    ngOnInit() {

        let ordersModel: OrdersModel = new OrdersModel();
        ordersModel.id = this.ordersId;
        setTimeout(() => {
            this.ordersService.ordersGet(ordersModel).then(res => {
                let dbOrdersModel: OrdersModel = res.source;
                this.orderModel = dbOrdersModel;
              
                
                this.baseOption.dataSource = dbOrdersModel;
                this.orderMemberOption.dataSource = dbOrdersModel.orderMemberBO;
                this.payPaymentOption.dataSource = dbOrdersModel.payPaymentBO;
                this.ordersGoodsOption.dataSource = dbOrdersModel.ordersItemBOList[0].goodsBO;
            })
        }, 200);
    }


    getUrl(item:OrdersItemModel):string{
        let urlJson = JSON.parse(item.goodsBO.coverUrl);
        return urlJson[0].url?urlJson[0].url:'';
    }



}
