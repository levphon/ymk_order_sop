import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { HttpService } from 'src/base/utlis/http.config';
import { ReqCode } from 'src/core/code';
import { CmdData } from 'src/core/module/CmdSignModel';
import { OrdersModel } from 'src/core/module/orders/orders.model';
import { OrdersCmdCode } from '../../core';
import { OrdersService } from '../../core/service/OrdersService';


@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class OrdersListComponent implements OnInit {

  constructor(
    private message:NzMessageService,
    private router: Router,
    private http:HttpService,
    private modal: NzModalService
  ) { }

  ordersService:OrdersService = new OrdersService(this.http);
  @ViewChild('myForm') myForm: NgsForm | any;
  @ViewChild('myTable') myTable: NgsTable | any;

  panelOption:NgsPanelOption={
    crumbs:[
      {
        text:'订单列表'
      }
    ],
    buttons:[
      {
        text:'导出',
        action:() => {
           
        }
      }
    ]
  }

  
  formOption:NgsFormOption = {
    showSearch:true,
    column:4,
    components:[
      { label:'订单编号', property:'title' ,comp:'NgsFormInput', },
      { label:'订单分类', property:'type' ,comp:'NgsFormSelect',dataSource:[{id:1,name:'正常订单'},{id:2,name:'服务订单'},{id:3,name:'定制订单'}],dsLabel:'name',dsValue:'id' },
    ]
  }

  cmdSign:CmdData={
    cmdCode:OrdersCmdCode.ORDERS_LIST
  };

  statusTemplate:Array<NgsTabColumnBadgeOption> = [
    { status:1,tagLabel:'已完成',tagColor:'#52c41a' },
    { status:2,tagLabel:'待支付',tagColor:'#999' },
    { status:3,tagLabel:'已关闭',tagColor:'#ff4d4f' }
  ]
  
  tableOption:NgsTableOption = {
    dataSource:this.cmdSign,
    selections:[
      { text:'全部',value:null },
      { text:'待支付',value:2 },
      { text:'已完成',value:1 },
      { text:'已关闭',value:3 }
    ],
    table:{
      columns:[
        { title:'订单编号',property:'orderNo',
          click:(item:OrdersModel) => {
              this.router.navigateByUrl(`orders/details/${item.id}`)
          },
          tagOptin:[
            {
              tagColor:(item:OrdersModel) => {
                if(item.type === 2){
                  return "#87d068";
                }
                if(item.type === 3){
                  return "#108ee9";
                }
                return "#2db7f5";
              },
              tagLabel:(item:OrdersModel) => {
                if(item.type === 2){
                  return "服";
                }
                if(item.type === 3){
                  return "定";
                }
                return "常";
              }
            },
            {
              hidden:(item:OrdersModel) => {
                return (item.profitAmount > 0)?true:false;
              },
              tagColor:(item:OrdersModel) => {
                return (item.profitAmount > 0)?"#f50":"";
              },
              tagLabel:(item:OrdersModel) => {
                return (item.superiorAmount > 0)?"分":"";
              }
            },
          ],
        },
        { title:'下单会员',property:'orderMember',
          click:(item:OrdersModel) => {

          },
          info:(item:OrdersModel) => {
            return item.orderMemberBO.name;
          },
          subOption:(item:OrdersModel) => {
            return item.orderMemberBO.mobile;
          },
        },
        { title:'支付金额',property:'amount',
          subOption:(item:OrdersModel) => {
            if(item.payType === 1){
              return "支付宝支付";
            }
            return "微信支付";
          },
        },
        { title:'获利金额',property:'profitAmount' },
        { title:'状态',property:'status' ,badgeOption:this.statusTemplate},
        
        { title:'下单时间',property:'createdDate',width:'120' },
        { title:'付款时间',property:'createdDate',width:'120' }
      ],
      op:{
        width:200,
        groupButtons:[
          {
            text:'编辑',
            buttons:[
              {
                text:'修改订单价格',
                action:(item:any) => {
                    this.isVisible = true;
                    this.ordersModel = item;
                }
              },
              {
                text:'删除商品',
                action:(item:any) => {
                  this.modal.create({
                    nzTitle: '是否删除该订单',
                    nzContent: '删除后无法恢复！',
                    nzClosable: false,
                    nzOnOk: () => {
                      this.ordersService.ordersDel(item).then(res => {
                        this.message.success("删除完成");
                        this.myTable.search();
                      });
                    }
                  });
                }
              },
            ],
          }
        ]
      },
      
    }
  }

  ngOnInit() {
  }

  onSearch(event:any){
    this.myTable.search(event);
  }

  isVisible:boolean = false;
  ordersModel:OrdersModel = new OrdersModel();
  amount:number = 0;
  onPatchOrdersAmount(){
    if(this.amount < 1){
      this.message.warning("修改后金额不能低于1");
      return;
    }
    let ordersBO:OrdersModel = new OrdersModel();
    ordersBO.id = this.ordersModel.id;
    ordersBO.amount = this.amount;
    this.ordersService.ordersPatch(ordersBO,ReqCode.PATCH_ORDERS_AMOUNT).then(res => {
      this.myTable.search();
      this.message.success("修改成功!");
        this.isVisible = false;
    })
  }
}
