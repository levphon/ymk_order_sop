import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MchUserListComponent } from './page/list/list.component';
import { MchUserEditComponent } from './page/edit/edit.component';

const routes: Routes = [
    { path: 'mchUser/list', component: MchUserListComponent },
    { path: 'mchUser/post', component: MchUserEditComponent },
    { path: 'mchUser/:id/edit', component: MchUserEditComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MchUserRoutingModule { }
