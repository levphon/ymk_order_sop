import { HttpService } from "src/base/utlis/http.config"
import { CmdSignModel } from "src/core/module/CmdSignModel";
import { MchUserCmdCode } from "../cmd.code";

export class MchUserService {

    constructor(private http: HttpService) { }

    public mchUserList(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchUserCmdCode.MCH_USER_LIST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public mchUserGet(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchUserCmdCode.MCH_USER_GET;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public mchUserPatch(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchUserCmdCode.MCH_USER_PATCH;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public mchUserPost(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchUserCmdCode.MCH_USER_POST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public mchUserDel(reqData: any, reqCode?: string): Promise<any> {
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = MchUserCmdCode.MCH_USER_DEL;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

}