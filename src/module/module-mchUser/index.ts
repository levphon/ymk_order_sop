import { NgModule } from '@angular/core';
import { NgsModule } from 'src/base/common/ngs';

import { MchUserEditComponent } from './page/edit/edit.component';
import { MchUserListComponent } from './page/list/list.component';
import { MchUserRoutingModule } from './routing.module';

import { CommonModule } from '@angular/common';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';


const DIRECTIVES = [MchUserEditComponent, MchUserListComponent]

@NgModule({
    declarations: DIRECTIVES,
    exports: DIRECTIVES,
    imports:
        [
            MchUserRoutingModule,
            NgsModule,
            CommonModule,
            NzEmptyModule,
            NzModalModule,
            NzCheckboxModule,
            FormsModule
        ],
    entryComponents: DIRECTIVES,
    providers: []
})
export class MchUserModule { }
