import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CmdData, CmdSignModel } from 'src/core/module/CmdSignModel';
import { HttpService } from 'src/base/utlis/http.config';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NzMessageService } from 'ng-zorro-antd/message';
import { MchUserService } from '../../code/service/MchUserService';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { NzModalService } from 'ng-zorro-antd/modal';
import { MchUserCmdCode } from '../../code/cmd.code';

@Component({
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class MchUserListComponent implements OnInit {

    constructor(
        protected http: HttpService,
        private router: Router,
        private message: NzMessageService,
        private modal: NzModalService
    ) { }

    mchUserService: MchUserService = new MchUserService(this.http);
    @ViewChild('myForm') myForm: NgsForm | any;
    @ViewChild('myTable') myTable: NgsTable | any;

    panelOption: NgsPanelOption = {
        crumbs: [
            {
                text: "员工列表",
            }],
        buttons: [
            {
                text: '新增',
                action: () => {
                    this.router.navigateByUrl(`/mchUser/post`)
                }
            }
        ]
    }


    formOption: NgsFormOption = {
        column: 4,
        showSearch: true,
        components: [
            { label: '会员名称', property: 'name', placeHolder: '会员名称', comp: 'NgsFormInput' },
            { label: '手机号码', property: 'mobile', placeHolder: '手机号码', comp: 'NgsFormInput' }
        ]
    }

    statusTemplate: Array<NgsTabColumnBadgeOption> = [
        { status: 'DISABLED', tagLabel: '已禁用', tagColor: 'red' },
        { status: 'NORMAL', tagLabel: '正常', tagColor: 'green' },
    ]

    cmdSign: CmdData = {
        cmdCode: MchUserCmdCode.MCH_USER_LIST
    };

    tableOption: NgsTableOption = {
        selections: [
            { text: '全部', value: null },
            { text: '已禁用', value: 'DISABLED' },
            { text: '正常', value: 'NORMAL' },
        ],
        dataSource: this.cmdSign,
        table: {
            columns: [
                {
                    title: '名称',
                    property: 'name'
                },
                {
                    title: '手机号',
                    property: 'mobile'
                },
                {
                    title: '状态',
                    property: 'status',
                    badgeOption: this.statusTemplate,
                    width: 180
                },
                {
                    title: '创建日期',
                    property: 'createdDate'
                },
            ],
            op: {
                width: '200px',
                buttons: [
                    {
                        text: '编辑',
                        action: (item: any) => {
                            this.router.navigateByUrl('mchUser/' + item.id + '/edit');
                        }
                    },
                    {
                        text: (item: any) => {
                            return item.status == 0 ? '启用' : '禁用';
                        },
                        action: (item: any) => {
                            let model: any = {};
                            model.id = item.id;
                            model.status = item.status == 0 ? 1 : 0;
                            this.mchUserService.mchUserPatch(model).then(res => {
                                this.message.success("修改完成");
                                this.myTable.search();
                            });
                        }
                    },
                    {
                        text: '删除',
                        action: (item: any) => {
                            this.modal.create({
                                nzTitle: '是否删除该',
                                nzContent: '删除后无法恢复！',
                                nzClosable: false,
                                nzOnOk: () => {
                                    this.mchUserService.mchUserDel(item).then(res => {
                                        this.message.success("删除完成");
                                        this.myTable.search();
                                    });
                                }
                            });
                        }
                    },
                ]
            }
        }
    }

    ngOnInit() { }

    onSearch(event: any) {
        this.myTable.search(event);
    }
}