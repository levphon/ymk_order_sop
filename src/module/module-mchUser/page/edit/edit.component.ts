import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { HttpService } from 'src/base/utlis/http.config';
import { MchUserService } from '../../code/service/MchUserService';


@Component({
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css']
})
export class MchUserEditComponent implements OnInit {

    constructor(
        private message: NzMessageService,
        private router: Router,
        private http: HttpService,
        private route: ActivatedRoute,
    ) { }

    mchUserService: MchUserService = new MchUserService(this.http);

    panelOption: NgsPanelOption = {
        crumbs: [
            {
                text: '员工列表',
                action: () => {
                    this.router.navigateByUrl(`/mchUser/list`)
                }
            },
            {
                text: '员工操作'
            }
        ],
        buttons: [
            {
                text: '保存',
                action: () => {
                    let myForm = this.myForm.getValue();

                    if (this.bizId) {
                        myForm.id = this.bizId;
                        this.mchUserService.mchUserPatch(myForm).then(res => {
                            this.router.navigateByUrl("/mchUser/list");
                        });
                    } else {
                        this.mchUserService.mchUserPost(myForm).then(res => {
                            this.router.navigateByUrl("/mchUser/list");
                        });
                    }

                }
            }
        ]
    }

    @ViewChild('myForm') myForm: NgsForm | any;


    formOption: NgsFormOption = {
        showSearch: false,
        column: 3,
        components: [
            { label: '名称', property: 'name', comp: 'NgsFormInput', require: true, placeHolder: '请输入名称' },
            { label: '手机号', property: 'mobile', comp: 'NgsFormInput', require: true, placeHolder: '请输入手机号' },
        ]
    }

    bizId: number | any;
    ngOnInit() {
        this.route.params.subscribe((params: any) => {
            if (params.id) {
                this.bizId = params.id;

                let model: any = {};
                model.id = params.id;
                this.mchUserService.mchUserGet(model).then(res => {
                    let data = res.source;
                    this.myForm.setValue(data);
                })
            }
        })
    }

}
