import { NgModule } from '@angular/core';
import { NgsModule } from 'src/base/common/ngs';
import { NzIconModule } from 'ng-zorro-antd/icon';

import { RbacEditComponent } from './page/edit/edit.component';
import { RbacGetComponent } from './page/get/get.component';
import { RbacListComponent } from './page/list/list.component';
import { PermListComponent } from './page/perm/perm.component';
import { RbacRoutingModule } from './routing.module';
import { CommonModule } from '@angular/common';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';

const DIRECTIVES = [RbacEditComponent, RbacGetComponent, RbacListComponent,PermListComponent]

@NgModule({
  declarations: DIRECTIVES,
  exports: DIRECTIVES,
  imports:
    [
      RbacRoutingModule,
      NgsModule,
      NzIconModule,
      CommonModule,
      NzEmptyModule,
      NzModalModule,
      NzCheckboxModule,
      FormsModule
    ],
  entryComponents: DIRECTIVES,
  providers: []
})
export class RbacModule { }
