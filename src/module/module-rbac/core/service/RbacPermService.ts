import { HttpService } from "src/base/utlis/http.config"
import { CmdSignModel } from "src/core/module/CmdSignModel";
import { SessionCmdCode } from "src/module/module-home/core";
import { RbacCmdCode } from "../cmd.code";

export class RbacPermService {

    constructor(private http :HttpService) {
    }

    public rbacPermList(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_PERM_LIST;
        cmdSign.source = reqData;
        cmdSign.returnStruct = "{id:'',type:'',name:'',code:'',icon:'',sort:'',parentId:'',isSop:''}";
        return this.http.exe(cmdSign);
    }

    public rbacPermGet(data:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_PERM_GET;
        cmdSign.source = data;
        return this.http.exe(cmdSign);
    }

    public rbacPermPost(data:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_PERM_POST;
        cmdSign.source = data;
        return this.http.exe(cmdSign);
    }

    public rbacPermPatch(data:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_PERM_PATCH;
        cmdSign.source = data;
        return this.http.exe(cmdSign);
    }

    public rbacPermDel(data:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_PERM_DEL;
        cmdSign.source = data;
        return this.http.exe(cmdSign);
    }

    public rbacRoleList(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_ROLE_LIST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        cmdSign.returnStruct = "{id:'',name:'',rbacPermList:{id:'',name:''}}"
        return this.http.exe(cmdSign);
    }

    public rbacRolePost(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_ROLE_POST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }
    
    public rbacRolePatch(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_ROLE_PATCH;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public rbacRoleGet(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_ROLE_GET;
        cmdSign.returnStruct = "{id:'',name:'',code:'',rbacPermBOList:{id:''}}"
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public rbacRoleDel(data:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = RbacCmdCode.RBAC_ROLE_DEL;
        cmdSign.source = data;
        return this.http.exe(cmdSign);
    }
}