export class RbacCmdCode {
    public static readonly RBAC_PERM_LIST = "RBAC_PERM_LIST";
    public static readonly RBAC_PERM_POST = "RBAC_PERM_POST";
    public static readonly RBAC_PERM_DEL = "RBAC_PERM_DEL";
    public static readonly RBAC_PERM_GET = "RBAC_PERM_GET";
    public static readonly RBAC_PERM_PATCH = "RBAC_PERM_PATCH";

    public static readonly RBAC_ROLE_LIST = "RBAC_ROLE_LIST";
    public static readonly RBAC_ROLE_POST = "RBAC_ROLE_POST";
    public static readonly RBAC_ROLE_DEL = "RBAC_ROLE_DEL";
    public static readonly RBAC_ROLE_GET = "RBAC_ROLE_GET";
    public static readonly RBAC_ROLE_PATCH = "RBAC_ROLE_PATCH";
}