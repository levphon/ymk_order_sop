import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RbacListComponent } from './page/list/list.component';
import { RbacGetComponent } from './page/get/get.component';
import { RbacEditComponent } from './page/edit/edit.component';
import { PermListComponent } from './page/perm/perm.component';

const routes: Routes = [
  { path: 'perm', component: PermListComponent },
  { path: 'role/list', component: RbacListComponent },
  { path: 'role/post', component: RbacEditComponent },
  { path: 'role/:id/edit', component: RbacEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RbacRoutingModule { }
