import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NgsBlockOption, NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { HttpService } from 'src/base/utlis/http.config';
import { ReqCode } from 'src/core/code/req.code';
import { RbacPermModel } from 'src/core/module/rbac/rbac-perm.model';
import { RbacRoleModel } from 'src/core/module/rbac/rbac-role.model';
import { RbacPermService } from '../../core/service/RbacPermService';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class RbacEditComponent implements OnInit {

  constructor(
    private router: Router,
    private message: NzMessageService,
    private http: HttpService,
    public route: ActivatedRoute,
  ) { }

  rbacService: RbacPermService = new RbacPermService(this.http);

  panelOption: NgsPanelOption = {
    crumbs: [
      {
        text: '角色列表',
        action: () => {
          this.router.navigateByUrl("/role/list")
        }
      },
      {
        text: '角色添加'
      }
    ],
    buttons: [
      {
        text: '保存',
        action: () => {
          let rbacRoleModel: RbacRoleModel = this.myForm.getValue();
          rbacRoleModel.permIdList = this.permMap;
          if (this.roleId) {
            rbacRoleModel.id = this.roleId;
            this.rbacService.rbacRolePatch(rbacRoleModel).then(res => {
              this.message.success("修改成功！");
              this.router.navigateByUrl("/role/list");
            })
          } else {
            this.rbacService.rbacRolePost(rbacRoleModel).then(res => {
              this.message.success("添加成功！");
              this.router.navigateByUrl("/role/list");
            })
          }


        }
      }
    ]
  }

  /**
   * 基本信息
   */
  @ViewChild('myForm') myForm: NgsForm | any;
  baseBlock: NgsBlockOption = {
    title: '基本信息'
  }
  formOption: NgsFormOption = {
    showSearch: false,
    column: 5,
    components: [
      { label: '角色名称', property: 'name', comp: 'NgsFormInput', require: true },
      { label: '角色Code', property: 'code', comp: 'NgsFormInput', require: true }
    ]
  }

  /**
   * 权限code
   */
  permBlock: NgsBlockOption = {
    title: '权限信息'
  }

  rbacPermList: Array<RbacPermModel> = [];
  roleId: number | any;
  ngOnInit() {

    this.route.params.subscribe((params: any) => {
      if (params.id) {
        this.roleId = params.id;
        let rbacRoleModel: RbacRoleModel = new RbacRoleModel();
        rbacRoleModel.id = params.id;
        this.rbacService.rbacRoleGet(rbacRoleModel).then(res => {
          let rbacRoleBO: RbacRoleModel = res.source;
          this.myForm.setValue(rbacRoleBO);
          rbacRoleBO.rbacPermBOList.forEach((res: RbacPermModel) => {
            this.permMap.push(res.id);
          });
          this.rbacPermInit();
        })
      } else {
        this.rbacPermInit();
      }
    })

  }

  rbacPermInit() {
    this.rbacService.rbacPermList({}).then((res) => {
      let permMap1 = new Map();
      let permMap2 = new Map();
      let permMap3 = new Map();
      res.source.forEach((element: RbacPermModel) => {
        if (this.check(element.id)) {
          element.check = true;
        }
        if (element.type === 1) {
          permMap1.set(element.id, element);
        }
        if (element.type === 2) {
          permMap2.set(element.id, element);
        }
        if (element.type === 3) {
          permMap3.set(element.id, element);
        }
      });
      for (var obj of permMap3) {

        let perm = obj[1];
        let permboos = permMap2.get(perm.parentId);
        if (permboos.children) {
          permboos.children.push(perm);
        } else {
          let children = [];
          children.push(perm);
          permboos.children = children;
        }
      }
      for (let obj of permMap2) {
        let perm = obj[1];
        let permboos = permMap1.get(perm.parentId);
        if (permboos) {
          if (permboos.children) {
            permboos.children.push(perm);
          } else {
            let children = [];
            children.push(perm);
            permboos.children = children;
          }
        }

      }
      for (let obj of permMap1) {
        let perm = obj[1];
        this.rbacPermList.push(perm);
      }
    })
  }

  permMap: Array<number> = [];
  log(value: number[]): void {
    this.permMap = value;
  }
  check(value: any) {
    return this.permMap.indexOf(value) > -1 ? true : false;
  }


}
