import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageModule, NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsFormOption, NgsPanelOption, NgsTableOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTabColumnBadgeOption, NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { HttpService } from 'src/base/utlis/http.config';
import { ReqCode } from 'src/core/code';
import { CmdData, CmdSignModel } from 'src/core/module/CmdSignModel';
import { RbacRoleModel } from 'src/core/module/rbac/rbac-role.model';
import { RbacCmdCode } from '../../core';
import { RbacPermService } from '../../core/service/RbacPermService';

@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class RbacListComponent implements OnInit {

  constructor(
    private message: NzMessageService,
    private router: Router,
    private http: HttpService,
    private modal: NzModalService
  ) { }

  rbacPermService: RbacPermService = new RbacPermService(this.http);
  @ViewChild('myForm') myForm: NgsForm | any;
  @ViewChild('myTable') myTable: NgsTable | any;

  panelOption: NgsPanelOption = {
    crumbs: [
      {
        text: '角色列表'
      }
    ],
    buttons: [
      {
        text: '添加',
        action: () => {
          this.router.navigateByUrl("/role/post")
        }
      }
    ]
  }

  formOption: NgsFormOption = {
    showSearch: true,
    column: 4,
    components: [
      { label: '角色名称', property: 'name', comp: 'NgsFormInput', }
    ]
  }

  cmdSign: CmdData = {
    cmdCode: RbacCmdCode.RBAC_ROLE_LIST
  };

  statusTemplate: Array<NgsTabColumnBadgeOption> = [
    { status: "DISABLED", tagLabel: '禁用', tagColor: 'red' },
    { status: "NORMAL", tagLabel: '正常', tagColor: 'blue' }
  ]

  tableOption: NgsTableOption = {
    dataSource: this.cmdSign,
    selections: [
      { text: '全部', value: null },
      { text: '启用', value: "NORMAL" },
      { text: '禁用', value: "DISABLED" }
    ],
    table: {
      columns: [
        { title: '角色名称', property: 'name' },
        { title: '角色code', property: 'code' },
        {
          title: '范围', property: 'mch',
          info: (item: any) => {
            return item.mchBO?item.mchBO.name:'运营平台';
          }
        },
        { title: '状态', property: 'status', badgeOption: this.statusTemplate },
        { title: '创建时间', property: 'createdDate' }
      ],
      op: {
        buttons: [
          {
            text: '修改',
            action: (item: any) => {
              this.router.navigateByUrl(`role/${item.id}/edit`);
            }
          },
          {
            text: '禁用',
            hidden: (item: RbacRoleModel) => {
              return item.status == 1 ? true : false;
            },
            action: (item: RbacRoleModel) => {
              let rbacRoleModel: RbacRoleModel = new RbacRoleModel();
              rbacRoleModel.id = item.id;
              rbacRoleModel.status = 0;
              this.rbacPermService.rbacRolePatch(rbacRoleModel, ReqCode.UPDATE_STATUS).then(res => {
                this.message.success("修改完成");
                this.myTable.search();
              });
            }
          },
          {
            text: '启用',
            hidden: (item: RbacRoleModel) => {
              return item.status == 0 ? true : false;
            },
            action: (item: RbacRoleModel) => {
              let rbacRoleModel: RbacRoleModel = new RbacRoleModel();
              rbacRoleModel.id = item.id;
              rbacRoleModel.status = 1;
              this.rbacPermService.rbacRolePatch(rbacRoleModel, ReqCode.UPDATE_STATUS).then(res => {
                this.message.success("修改完成");
                this.myTable.search();
              });
            }
          },
          {
            text: '删除',
            action: (item: any) => {
              this.modal.create({
                nzTitle: '是否删除该角色',
                nzContent: '删除后无法恢复！',
                nzClosable: false,
                nzOnOk: () => {
                  this.rbacPermService.rbacRoleDel(item).then(res => {
                    this.message.success("删除完成");
                    this.myTable.search();
                  });

                }
              });
            }
          },
        ]
      }
    }
  }

  ngOnInit() {
  }

  onSearch(event: any) {
    this.myTable.search(event);
  }

}
