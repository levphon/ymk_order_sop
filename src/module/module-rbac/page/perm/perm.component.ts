import { Component, OnInit, ViewChild } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/form/ngs-form';
import { HttpService } from 'src/base/utlis/http.config';
import { CmdSignModel } from 'src/core/module/CmdSignModel';
import { RbacPermModel } from 'src/core/module/rbac/rbac-perm.model';
import { RbacCmdCode } from '../../core';
import { RbacPermService } from '../../core/service/RbacPermService';

@Component({
  templateUrl: './perm.component.html',
  styleUrls: ['./perm.component.scss']
})
export class PermListComponent implements OnInit {

  constructor(
    private httpClent :HttpService,
    private message:NzMessageService,
    private modal: NzModalService
    ) { }
    rbacPermService = new RbacPermService(this.httpClent);
  
  panelOption:NgsPanelOption = {
      crumbs:[
        {
          text:'权限管理'
        }
      ]
  };

  oneData:Array<RbacPermModel>=[];
  twoData:Array<RbacPermModel>=[];
  threeData:Array<RbacPermModel>=[];
  style = {
    'background-color':'#108ee9 !important',
    'color':'#FFFFFF'
  };


  ngOnInit() {
    this.init(0,1);
  }

  init(parentId:number,type:number){
    let rbacPermModel = new RbacPermModel();
    rbacPermModel.type = type;
    rbacPermModel.parentId = parentId;
    this.rbacPermService.rbacPermList(rbacPermModel).then(res => {
      if(type === 1){
        this.oneData = res.source;
      }else if(type === 2){
        this.twoData = res.source;
      }
      
    })
  }
  

  // 一级菜单点击
  clickOne(item:any,i:number){
    this.oneData.forEach((element:any) => {
      element.style = '';
    });
    this.oneData[i].style = this.style;
    let rbacPermModel = new RbacPermModel();
    rbacPermModel.type = 2;
    rbacPermModel.parentId = item.id;
    this.rbacPermService.rbacPermList(rbacPermModel).then(res => {
      this.twoData = res.source;
      this.permTwoData.parentId = item.id;
    })
  }

  // 二级菜单点击
  clickTwo(item:any,i:number){
    this.twoData.forEach((element:any) => {
      element.style = '';
    });
    this.twoData[i].style = this.style;
  }

  addPerm(type:number){
    if(type === 1){
      this.permData.isVisible = true;
    }else if(type === 2){
      this.permTwoData.isVisible = true;
    }
  }

  edit(rbacPermModel:RbacPermModel){
    if(rbacPermModel.type == 1){
      this.permData.isVisible = true;
      this.permData.id = rbacPermModel.id;
      this.permData.type=2;
  
      setTimeout(() => {
        this.permForm.setValue(rbacPermModel);
      }, 100);
    }else if(rbacPermModel.type == 2){
      this.permTwoData.isVisible = true;
      this.permTwoData.type=2;
  
      setTimeout(() => {
        this.permTwoForm.setValue(rbacPermModel);
      }, 100);
    }

  }

  del(item:RbacPermModel){
    this.modal.create({
      nzTitle: '删除权限',
      nzContent: '确认删除么？该权限下的所有子权限也会删除!',
      nzClosable: false,
      nzOnOk: () => {
        this.rbacPermService.rbacPermDel(item).then(res => {
          this.message.success("删除成功!");
          this.init(item.parentId,item.type);
        });
      }
    });
  }

  
  /**
   * 一级菜单操作
   */
  permFormOption:NgsFormOption = {
    showSearch:false,
    column:1,
    labelSpan:4,
    compSpan:20,
    components:[
      { label:'权限名称',property:'name',comp:'NgsFormInput',require:true },
      { label:'权限图标',property:'icon',comp:'NgsFormInput',require:true },
      { label:'权限范围',property:'isSop',comp:'NgsFormRadio',dataSource:[{id:0,value:"商户端"},{id:1,value:"运营端"}],dsLabel:'value',dsValue:'id' },
      { label:'排序',type:'number',property:'sort',comp:'NgsFormInput',require:true },
    ]
  }
  @ViewChild('permForm') permForm: NgsForm | any;

  permData={
    isVisible:false,
    id:0,
    parentId:0,
    type:1
  }
  handleCancel(){
    this.permData.isVisible = false;
  }
  handleOk(){
    let rbacPermModel = new RbacPermModel();
    
    rbacPermModel = this.permForm.getValue();
    console.log(rbacPermModel)
    if(this.permData.type === 1){
      rbacPermModel.type = 1;
      rbacPermModel.parentId = 0;
      this.rbacPermService.rbacPermPost(rbacPermModel).then(res => {
        this.message.success("添加成功!");
        this.permData.isVisible = false;
        this.init(0,1);
      })
    }else{
      rbacPermModel.type = 1;
      rbacPermModel.parentId = 0;
      rbacPermModel.id = this.permData.id;
      this.rbacPermService.rbacPermPatch(rbacPermModel).then(res => {
        this.message.success("修改成功!");
        this.permData.isVisible = false;
        this.init(0,1);
      })
    }
  }

  /**
   * 二级菜单操作
   */
   permTwoFormOption:NgsFormOption = {
    showSearch:false,
    column:1,
    labelSpan:4,
    compSpan:20,
    components:[
      { label:'权限名称',property:'name',comp:'NgsFormInput',require:true },
      { label:'权限Code',property:'code',comp:'NgsFormInput',require:true },
      { label:'权限范围',property:'isSop',comp:'NgsFormRadio',dataSource:[{id:0,value:"商户端"},{id:1,value:"运营端"}],dsLabel:'value',dsValue:'id' },
      { label:'排序',type:'number',property:'sort',comp:'NgsFormInput',require:true },
    ]
  }
  @ViewChild('permTwoForm') permTwoForm: NgsForm | any;

  permTwoData={
    isVisible:false,
    parentId:0,
    type:2
  }
  handleTwoCancel(){
    this.permTwoData.isVisible = false;
  }
  handleTwoOk(){
    let rbacPermModel = new RbacPermModel();
    rbacPermModel = this.permTwoForm.getValue();
    rbacPermModel.type = 2;
    rbacPermModel.parentId = this.permTwoData.parentId;
    this.rbacPermService.rbacPermPost(rbacPermModel).then(res => {
      this.message.success("添加成功!");
      this.permTwoData.isVisible = false;
      this.init(this.permTwoData.parentId,2);
    })
  }




}
