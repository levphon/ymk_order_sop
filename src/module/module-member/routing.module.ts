import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MemberListComponent } from './page/list/list.component';
import { MemberGetComponent } from './page/get/get.component';
import { MemberEditComponent } from './page/edit/edit.component';

const routes: Routes = [
  { path: 'member/list', component: MemberListComponent },
  { path: 'member/:id/get', component: MemberGetComponent },
  { path: 'member/:id/edit', component: MemberEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule { }
