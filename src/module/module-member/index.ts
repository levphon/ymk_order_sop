import { NgModule } from '@angular/core';
import { NgsModule } from 'src/base/common/ngs';

import { MemberEditComponent } from './page/edit/edit.component';
import { MemberGetComponent } from './page/get/get.component';
import { MemberListComponent } from './page/list/list.component';
import { MemberRoutingModule } from './routing.module';
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';

const DIRECTIVES = [MemberEditComponent, MemberGetComponent, MemberListComponent]

registerLocaleData(zh);

@NgModule({
  declarations: DIRECTIVES,
  exports: DIRECTIVES,
  imports:
    [
      MemberRoutingModule,
      NgsModule
    ],
  entryComponents: DIRECTIVES,
  providers: []
})
export class MemberModule { }
