import { Component, OnInit, ViewChild } from '@angular/core';
import { CmdData, CmdSignModel } from 'src/core/module/CmdSignModel';
import { HttpService } from 'src/base/utlis/http.config';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NzMessageService } from 'ng-zorro-antd/message';
import { MemberCmdCode } from '../../core/cmd.code';
import { MemberModel } from 'src/core/module/member/member.model';
import { MchModel } from 'src/core/module/mch/mch.model';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';


@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class MemberListComponent implements OnInit {
  router: any;

  constructor(
    protected http: HttpService,
    private msg: NzMessageService
  ) { }

  @ViewChild('myForm') myForm: NgsForm | any;
  @ViewChild('myTable') myTable: NgsTable | any;

  panelOption: NgsPanelOption = {
    crumbs: [
      {
        text: "会员列表",
      }
    ],
    buttons: [
    ]
  }


  formOption: NgsFormOption = {
    column: 4,
    showSearch: true,
    components: [
      { label: '会员名称', property: 'name', placeHolder: '会员名称', comp: 'NgsFormInput' },
      { label: '手机号码', property: 'mobile', placeHolder: '手机号码', comp: 'NgsFormInput' }
    ]
  }

  statusTemplate: Array<NgsTabColumnBadgeOption> = [
    { status: 0, tagLabel: '禁用', tagColor: 'red' },
    { status: 1, tagLabel: '正常', tagColor: 'green' }
  ]


  cmdSign: CmdData = {
    cmdCode: MemberCmdCode.MEMBER_LIST
  };
  tabOption: NgsTableOption = {
    selections: [
      { text: '全部', value: null },
      { text: '正常', value: 1 },
      { text: '禁用', value: 0 }
    ],
    dataSource: this.cmdSign,
    table: {
      columns: [
        {
          title: '商户',
          property: 'mch',
          info: (item: MemberModel) => {
            return item.mchBO.title;
          }
        },
        {
          title: '姓名',
          property: 'name',
          click: (data: any) => {
            console.log(data)
          },
          subOption: (res: MemberModel) => {
            return res.mobile;
          }
        },
        { title: '消费', property: 'totalConsumption', width: 180 },
        { title: '状态', property: 'status', badgeOption: this.statusTemplate, width: 180 },
        { title: '注册时间', property: 'createdDate' },
      ],
      op: {
        width: '200px',
        buttons: [
          {
            text: (res: MemberModel) => {
              if (res.status) {
                return "停用";
              }
              return "启用";
            },
            action: (data: any) => {
              console.log("点击了很多");
              console.log(data);
            }
          }
        ]
      }
    }
  }

  ngOnInit() {

  }

  onSearch(event: any) {
    this.myTable.search(event);
  }

  submit(value: {}): void {
    this.msg.success(JSON.stringify(value));
  }

}
