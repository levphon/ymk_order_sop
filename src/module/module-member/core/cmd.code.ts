export class MemberCmdCode {
  
    public static readonly MEMBER_GET = "MEMBER_GET";
    public static readonly MEMBER_LIST = "MEMBER_LIST";
    public static readonly MEMBER_POST = "MEMBER_POST";
    public static readonly MEMBER_PATCH = "MEMBER_PATCH";
    public static readonly MEMBER_DEL = "MEMBER_DEL";

    public static readonly MEMBER_FINANCE_GET = "MEMBER_FINANCE_GET";
    public static readonly MEMBER_FINANCE_LIST = "MEMBER_FINANCE_LIST";
    public static readonly MEMBER_FINANCE_POST = "MEMBER_FINANCE_POST";
    public static readonly MEMBER_FINANCE_PATCH = "MEMBER_FINANCE_PATCH";
    public static readonly MEMBER_FINANCE_DEL = "MEMBER_FINANCE_DEL";

}