import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpService } from 'src/base/utlis/http.config';
import { UserModel } from 'src/core/module/user/user.model';
import { UserService } from 'src/core/service';
import { CoreService } from 'src/core/service/core.service';


@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private message:NzMessageService,
    private coreService:CoreService,
    private http :HttpService,
    private router: Router
  ) { }

  userService = new UserService(this.http);
  userModuel:UserModel = new UserModel();

  ngOnInit() {
  }

  login(){
    if(!this.userModuel.mobile){
      this.message.warning("用户名必填!");
      return;
    }
    if(!this.userModuel.password){
      this.message.warning("密码必填!");
      return;
    }

    this.userService.userSessionPost(this.userModuel).then(res=>{
      sessionStorage.setItem("ADMIN_JWT_NAME",res.source.token);
      console.log(res.source)
      this.coreService.setUserModel(res.source);
      this.message.success("登陆成功!");
      this.router.navigateByUrl("/");
    })
  }

}
