import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { HttpService } from 'src/base/utlis/http.config';
import { UserModel } from 'src/core/module/user/user.model';
import { UserService } from 'src/core/service';
import { CoreService } from 'src/core/service/core.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
  isCollapsed = false;
  constructor(
    private router: Router,
    private coreService: CoreService,
    private http:HttpService,
    protected message: NzMessageService
  ) {
  }
  userService = new UserService(this.http);
  userModel: UserModel = this.coreService.getUserModel();

  ngOnInit(): void {
  }

  logout() {
    sessionStorage.removeItem("ADMIN_JWT_NAME");
		this.router.navigateByUrl('/login');
	}
  
}
