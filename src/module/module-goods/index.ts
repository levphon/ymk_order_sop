import { NgModule } from '@angular/core';
import { NgsModule } from 'src/base/common/ngs';

import { GoodsEditComponent } from './page/edit/edit.component';
import { GoodsGetComponent } from './page/get/get.component';
import { GoodsListComponent } from './page/list/list.component';
import { GoodsCatComponent } from './page/cat/cat.component';
import { GoodsRoutingModule } from './routing.module';
import { NzTabsModule } from 'ng-zorro-antd/tabs';

import { CommonModule } from '@angular/common';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';

const DIRECTIVES = [GoodsEditComponent, GoodsGetComponent, GoodsListComponent, GoodsCatComponent]

@NgModule({
  declarations: DIRECTIVES,
  exports: DIRECTIVES,
  imports:
    [
      GoodsRoutingModule,
      NgsModule,
      CommonModule,
      NzEmptyModule,
      NzModalModule,
      NzCheckboxModule,
      FormsModule,
      NzTabsModule,
      NzInputNumberModule,
      NzTableModule,
      NzInputModule,
      NzButtonModule,
      NzIconModule
    ],
  entryComponents: DIRECTIVES,
  providers: []
})
export class GoodsModule { }
