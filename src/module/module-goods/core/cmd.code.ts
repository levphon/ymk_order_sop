export class GoodsCmdCode {
  
    public static readonly GOODS_CAT_GET = "GOODS_CAT_GET";
    public static readonly GOODS_CAT_LIST = "GOODS_CAT_LIST";
    public static readonly GOODS_CAT_POST = "GOODS_CAT_POST";
    public static readonly GOODS_CAT_PATCH = "GOODS_CAT_PATCH";
    public static readonly GOODS_CAT_DEL = "GOODS_CAT_DEL";

    public static readonly GOODS_GET = "GOODS_GET";
    public static readonly GOODS_LIST = "GOODS_LIST";
    public static readonly GOODS_POST = "GOODS_POST";
    public static readonly GOODS_PATCH = "GOODS_PATCH";
    public static readonly GOODS_DEL = "GOODS_DEL";

    public static readonly GOODS_CACHE = "GOODS_CACHE";

}