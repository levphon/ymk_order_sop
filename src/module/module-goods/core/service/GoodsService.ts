import { HttpService } from "src/base/utlis/http.config"
import { CmdSignModel } from "src/core/module/CmdSignModel";
import { GoodsCmdCode } from "../cmd.code";

export class GoodsService {

    constructor(private http :HttpService) {
    }

    public goodsCatGet(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_CAT_GET;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsCatList(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_CAT_LIST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsCatPatch(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_CAT_PATCH;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsCatPost(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_CAT_POST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsCatDel(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_CAT_DEL;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsGet(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_GET;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsList(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_LIST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsPatch(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_PATCH;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsPost(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_POST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsDel(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_DEL;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public goodsCache(reqData:any):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = GoodsCmdCode.GOODS_CACHE;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }
    
}