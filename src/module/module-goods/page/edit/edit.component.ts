import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { HttpService } from 'src/base/utlis/http.config';
import { CmdData } from 'src/core/module/CmdSignModel';
import { GoodsCatPropModel } from 'src/core/module/goods/goods-cat-prop.model';
import { GoodsCatModel } from 'src/core/module/goods/goods-cat.model';
import { GoodsSkuPropModel } from 'src/core/module/goods/goods-sku-prop.model';
import { GoodsModel } from 'src/core/module/goods/goods.model';
import { MchCmdCode } from 'src/module/module-mch/code/cmd.code';
import { GoodsCmdCode } from '../../core';
import { GoodsService } from '../../core/service/GoodsService';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class GoodsEditComponent implements OnInit {

  constructor(
    private message: NzMessageService,
    private router: Router,
    private http: HttpService,
    private route: ActivatedRoute,
  ) { }

  goodsService: GoodsService = new GoodsService(this.http);

  panelOption: NgsPanelOption = <NgsPanelOption>{
    crumbs: [
      {
        text: '商品列表',
        action: () => {
          this.router.navigateByUrl(`/goods/list`)
        }
      },
      {
        text: '商品添加'
      }
    ],
    buttons: [
      {
        text: '添加',
        action: () => {
          /**基本信息 */
          let goodsModule: GoodsModel = this.baseForm.getValue();
          /**图片信息 */
          let imageData = this.imageForm.getValue();
          goodsModule.coverUrl = imageData.coverUrl;


          if (this.goodsId) {
            goodsModule.id = this.goodsId;
            this.goodsService.goodsPatch(goodsModule).then(res => {
              this.message.success("修改成功!");
              this.router.navigateByUrl("/goods/list");
            })
          } else {
            this.goodsService.goodsPost(goodsModule).then(res => {
              this.message.success("添加成功!");
              this.router.navigateByUrl("/goods/list");
            })
          }

        }
      }
    ]
  }

  /** 基本信息 */
  @ViewChild('baseForm') baseForm: NgsForm | any;

  calCatCmdSign: CmdData = {
    cmdCode: GoodsCmdCode.GOODS_CAT_LIST,
    source: { status: 'NORMAL' },
    returnStruct: "{id:'',name:''}"
  };

  mchGetCmdSign: CmdData = {
    cmdCode: MchCmdCode.MCH_LIST,
    source: { status: 'NORMAL' },
    returnStruct: "{id:'',title:''}"
  };

  baseOption: NgsFormOption = <NgsFormOption>{
    showSearch: false,
    column: 3,
    components: [
      { label: '商品名称', property: 'title', comp: 'NgsFormInput', placeHolder: '商品名称', require: true },
      {
        label: '商品标签', property: 'catId', comp: 'NgsFormSelect',
        dataSource: this.calCatCmdSign, dsLabel: 'name', dsValue: 'id',
        dsSubValue: (item: GoodsCatModel) => {
          return item.mchBO ? item.mchBO.title : "-";
        },
        placeHolder: '商品标签', require: true
      },
      {
        label: '归属商户', property: 'mchId', comp: 'NgsFormSelect',
        dataSource: this.mchGetCmdSign, dsLabel: 'title', dsValue: 'id',
        placeHolder: '归属商户', require: true
      },
      { label: '销售价格', property: 'price', comp: 'NgsFormInput', type: 'number', placeHolder: '销售价格', require: true },
      { label: '成本价格', property: 'costPrice', comp: 'NgsFormInput', type: 'number', placeHolder: '宣传价格', require: true },
      { label: '宣传价格', property: 'salePrice', comp: 'NgsFormInput', type: 'number', placeHolder: '宣传价格', require: true },
      { label: '商品备注', property: 'remark', comp: 'NgsFormInput', placeHolder: '商品备注', require: true },
    ]
  }

  /** 图片信息 */
  @ViewChild('imageForm') imageForm: NgsForm | any;
  imageOption: NgsFormOption = <NgsFormOption>{
    showSearch: false,
    column: 1,
    compSpan: 22,
    labelSpan: 2,
    components: [
      { label: '封面', property: 'coverUrl', comp: 'NgsFormUploader', data: [], accept: 'image' }
    ]
  }


  goodsId: number | any;
  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      if (params.id) {
        this.goodsId = params.id;
        let goodsModel: GoodsModel = new GoodsModel();
        goodsModel.id = params.id;
        setTimeout(() => {
          this.goodsService.goodsGet(goodsModel).then(res => {
            let goodsModel: GoodsModel = res.source;
            this.baseForm.setValue(goodsModel);
            let imageData = {
              coverUrl: goodsModel.coverUrl
            }
            this.imageForm.setValue(imageData);
          })
        }, 200);
        this.panelOption.crumbs[1].text = "商品修改";
        if (this.panelOption.buttons && this.panelOption.buttons[0]) {
          this.panelOption.buttons[0].text = "修改";
        }
        
        this.initCatProp(params.id);
      }
    })
  }

  style = {
    'background-color':'#108ee9 !important',
    'color':'#FFFFFF'
  };
  catProp:GoodsCatPropModel = new GoodsCatPropModel();
  catPropData:Array<GoodsCatPropModel> = [this.catProp];
  skuPropData:Array<GoodsSkuPropModel> = [];

  initCatProp(goodsId:number){

  }
  clickCatProp(item:GoodsCatPropModel,i:number){
    console.log(this.catPropData[i])
    this.catPropData[i].style = this.style;
  }
  addCatProp(){
    let newCatProp:GoodsCatPropModel = new GoodsCatPropModel();
    this.catPropData.push(newCatProp);
    console.log(this.catPropData)
  }
  delCatProp(item:GoodsCatPropModel){
    
  }
  editCatProp(item:GoodsCatPropModel){

  }
  delSkuProp(item:GoodsSkuPropModel){

  }
  editSkuProp(item:GoodsSkuPropModel){

  }
}


