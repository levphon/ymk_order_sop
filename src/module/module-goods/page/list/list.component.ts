import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { HttpService } from 'src/base/utlis/http.config';
import { ReqCode } from 'src/core/code';
import { CmdData } from 'src/core/module/CmdSignModel';
import { GoodsCatModel } from 'src/core/module/goods/goods-cat.model';
import { GoodsModel } from 'src/core/module/goods/goods.model';
import { RbacRoleModel } from 'src/core/module/rbac/rbac-role.model';
import { UserModel } from 'src/core/module/user/user.model';
import { MchCmdCode } from 'src/module/module-mch/code/cmd.code';
import { GoodsCmdCode } from '../../core';
import { GoodsService } from '../../core/service/GoodsService';


@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class GoodsListComponent implements OnInit {

  constructor(
    private message: NzMessageService,
    private router: Router,
    private http: HttpService,
    private modal: NzModalService
  ) { }

  goodsService: GoodsService = new GoodsService(this.http);
  @ViewChild('myForm') myForm: NgsForm | any;
  @ViewChild('myTable') myTable: NgsTable | any;

  panelOption: NgsPanelOption = {
    crumbs: [
      {
        text: '商品列表'
      }
    ],
    buttons: [
      {
        text: '添加',
        action: () => {
          this.router.navigateByUrl(`/goods/post`)
        }
      }
    ]
  }

  roleCmdSign: CmdData = {
    cmdCode: GoodsCmdCode.GOODS_CAT_LIST
  };
  mchGetCmd: CmdData = {
    cmdCode: MchCmdCode.MCH_LIST
  }
  formOption: NgsFormOption = {
    showSearch: true,
    column: 4,
    components: [
      { label: '商品名称', property: 'title', comp: 'NgsFormInput', placeHolder: '商品名称' },
      { label: '商品标签', property: 'catId', comp: 'NgsFormSelect', dataSource: this.roleCmdSign, 
        dsLabel: 'name', dsValue: 'id',dsSubValue:(item:GoodsCatModel) => {
          return item.mchBO?item.mchBO.title:"-";
        }, placeHolder: '商品标签' },
      { label: '归属商户', property: 'mchId', comp: 'NgsFormSelect', dataSource: this.mchGetCmd, dsLabel: 'title', dsValue: 'id', placeHolder: '归属商户' }
    ]
  }

  cmdSign: CmdData = {
    cmdCode: GoodsCmdCode.GOODS_LIST
  };

  statusTemplate: Array<NgsTabColumnBadgeOption> = [
    { status: "DISABLED", tagLabel: '已下架', tagColor: 'red' },
    { status: "NORMAL", tagLabel: '已上架', tagColor: 'blue' }
  ]

  tableOption: NgsTableOption = {
    dataSource: this.cmdSign,
    selections: [
      { text: '全部', value: null },
      { text: '已上架', value: "NORMAL" },
      { text: '已下架', value: "DISABLED" }
    ],
    table: {
      columns: [
        {
          title: '商户',
          property: 'mch',
          width: 80,
          info: (item: GoodsModel) => {
            return item.mchBO ? item.mchBO.title : "-";
          }
        },
        {
          title: '商品标题', property: 'title',
          subOption: (item: GoodsModel) => {
            return "商品ID:" + item.id + "   热度:" + item.heat;
          }
        },
        {
          title: '商品分类', property: 'catId', width: 200,
          info: (item: GoodsModel) => {
            return item.goodsCatBO ? item.goodsCatBO.name : '暂无';
          }
        },
        { title: '价格', property: 'price', width: 80 },
        { title: '成本价', property: 'costPrice', width: 80 },
        { title: '销量', property: 'salesVolume', width: 80 },
        { title: '状态', property: 'status', badgeOption: this.statusTemplate, width: 180 },
        { title: '创建时间', property: 'createdDate', width: 200 }
      ],
      op: {
        width: 200,
        groupButtons: [
          {
            text: '编辑',
            buttons: [
              {
                text: '修改商品',
                action: (item: any) => {
                  this.router.navigateByUrl(`goods/${item.id}/edit`);
                }
              },
              {
                text: '修改热度',
                action: (item: GoodsModel) => {
                  this.goodsModel = item;
                  this.isVisible = true;
                }
              },
              {
                text: (item: GoodsModel) => {
                  return item.status == 'NORMAL' ? "下架商品" : "上架商品";
                },
                action: (item: GoodsModel) => {
                  let goods: GoodsModel = new GoodsModel();
                  goods.id = item.id;
                  goods.status = item.status == 'NORMAL' ? 'DISABLED' : 'NORMAL';
                  this.goodsService.goodsPatch(goods, ReqCode.UPDATE_STATUS).then(res => {
                    this.message.success("修改完成");
                    this.myTable.search();
                  });
                }
              },
              {
                text: '删除商品',
                action: (item: any) => {
                  this.modal.create({
                    nzTitle: '是否删除该用户',
                    nzContent: '删除后无法恢复！',
                    nzClosable: false,
                    nzOnOk: () => {
                      this.goodsService.goodsDel(item).then(res => {
                        this.message.success("删除完成");
                        this.myTable.search();
                      });
                    }
                  });
                }
              },
            ],
          },
          {
            text: '同步',
            buttons: [
              {
                text: '同步缓存',
                action: (item: GoodsModel) => {
                  let goodsModel: GoodsModel = new GoodsModel();
                  goodsModel.id = item.id;
                  this.goodsService.goodsCache(goodsModel).then(res => {
                    this.message.success("同步完成");
                  });
                }
              },
            ],
          }
        ]
      },

    }
  }

  ngOnInit() {
  }

  onSearch(event: any) {
    this.myTable.search(event);
  }

  // 修改热度
  isVisible: boolean = false;
  goodsModel: GoodsModel = new GoodsModel();
  heat: number = 0;
  editHeatOk() {
    let goodsModel: GoodsModel = new GoodsModel();
    goodsModel.id = this.goodsModel.id;
    goodsModel.heat = this.heat;

    this.goodsService.goodsPatch(goodsModel, ReqCode.UPDATE_HEAT).then(res => {
      this.message.success("修改成功!");
      this.isVisible = false;
      this.myTable.search();
    })
  }
}
