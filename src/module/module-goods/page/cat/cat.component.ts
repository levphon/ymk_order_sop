import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm, NgsFormOption } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTabColumnBadgeOption, NgsTable, NgsTableOption } from 'src/base/common/ngs/table/ngs-table-module';
import { HttpService } from 'src/base/utlis/http.config';
import { ReqCode } from 'src/core/code';
import { CmdData } from 'src/core/module/CmdSignModel';
import { GoodsCatModel } from 'src/core/module/goods/goods-cat.model';
import { MchCmdCode } from 'src/module/module-mch/code/cmd.code';
import { GoodsCmdCode } from '../../core';
import { GoodsService } from '../../core/service/GoodsService';

@Component({
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.css']
})
export class GoodsCatComponent implements OnInit {

  constructor(
    private message: NzMessageService,
    private router: Router,
    private http: HttpService,
    private modal: NzModalService
  ) { }

  goodsService: GoodsService = new GoodsService(this.http);
  @ViewChild('myForm') myForm: NgsForm | any;
  @ViewChild('myTable') myTable: NgsTable | any;

  panelOption: NgsPanelOption = {
    crumbs: [
      {
        text: '商品标签'
      }
    ],
    buttons: [
      {
        text: '添加',
        action: () => {
          this.editData.type = 1;
          this.editData.isVisible = true;
        }
      }
    ]
  }

  mchGetCmd: CmdData = {
    cmdCode: MchCmdCode.MCH_LIST
  }
  formOption: NgsFormOption = {
    showSearch: true,
    column: 4,
    components: [
      { label: '标签名称', property: 'name', comp: 'NgsFormInput', placeHolder: '标签名称' },
      {
        label: '归属商户', property: 'mchId', comp: 'NgsFormSelect',
        dataSource: this.mchGetCmd, dsLabel: 'title', dsValue: 'id',
        openSerch: true, placeHolder: '归属商户'
      },
    ]
  }

  cmdSign: CmdData = {
    cmdCode: GoodsCmdCode.GOODS_CAT_LIST
  };

  statusTemplate: Array<NgsTabColumnBadgeOption> = [
    { status: "DISABLED", tagLabel: '禁用', tagColor: 'red' },
    { status: "NORMAL", tagLabel: '正常', tagColor: 'blue' }
  ]

  tableOption: NgsTableOption = {
    dataSource: this.cmdSign,
    selections: [
      { text: '全部', value: null },
      { text: '启用', value: "NORMAL" },
      { text: '禁用', value: "DISABLED" }
    ],
    table: {
      columns: [
        {
          title: '标签名称',
          property: 'name',
          subOption: (item: GoodsCatModel) => {
            return item.mchBO ? item.mchBO.title : "-";
          }
        },
        { title: '排序', property: 'sort' },
        { title: '状态', property: 'status', badgeOption: this.statusTemplate },
        { title: '创建时间', property: 'createdDate' }
      ],
      op: {
        buttons: [
          {
            text: '修改',
            action: (item: any) => {
              let goodsCatModel: GoodsCatModel = new GoodsCatModel();
              goodsCatModel.id = item.id;
              this.goodsService.goodsCatGet(goodsCatModel).then(res => {
                this.editData.isVisible = true;
                this.editData.type = 2;
                this.editData.id = item.id;
                setTimeout(() => {
                  this.editForm.setValue(res.source);
                }, 100);

              })
            }
          },
          {
            text: (item: GoodsCatModel) => {
              return item.status == "NORMAL" ? "禁用" : "启用";
            },
            action: (item: GoodsCatModel) => {
              let rbacRoleModel: GoodsCatModel = new GoodsCatModel();
              rbacRoleModel.id = item.id;
              rbacRoleModel.status = "NORMAL";
              if (item.status == "NORMAL") {
                rbacRoleModel.status = "DISABLED";
              }
              this.goodsService.goodsCatPatch(rbacRoleModel, ReqCode.UPDATE_STATUS).then(res => {
                this.message.success("修改完成");
                this.myTable.search();
              });
            }
          },
          {
            text: '删除',
            action: (item: any) => {
              this.modal.create({
                nzTitle: '是否删除该用户',
                nzContent: '删除后无法恢复！',
                nzClosable: false,
                nzOnOk: () => {
                  this.goodsService.goodsCatDel(item).then(res => {
                    this.message.success("删除完成");
                    this.myTable.search();
                  });

                }
              });
            }
          },
        ]
      }
    }
  }

  ngOnInit() {
  }

  onSearch(event: any) {
    this.myTable.search(event);
  }

  mchGetCmdForm: CmdData = {
    cmdCode: MchCmdCode.MCH_LIST,
    source: { status: 'NORMAL' }
  }
  @ViewChild('editForm') editForm: NgsForm | any;
  editFormOption: NgsFormOption = {
    showSearch: false,
    column: 1,
    labelSpan: 4,
    compSpan: 20,
    components: [
      { label: '标签名称', property: 'name', comp: 'NgsFormInput', placeHolder: '标签名称', require: true },
      { label: '标签排序', property: 'sort', comp: 'NgsFormInput', placeHolder: '标签排序', require: true },
      {
        label: '归属商户', property: 'mchId', comp: 'NgsFormSelect',
        dataSource: this.mchGetCmdForm, dsLabel: 'title', dsValue: 'id',
        openSerch: true, placeHolder: '归属商户', require: true
      }
    ]
  }
  editData = {
    isVisible: false,
    type: 1,
    id: 0
  }
  editOk() {
    let goodsCatModel: GoodsCatModel = this.editForm.getValue();
    if (this.editData.type == 1) {
      this.goodsService.goodsCatPost(goodsCatModel).then(res => {
        this.message.success("添加完成!");
        this.myTable.search();
        this.editData.isVisible = false;

      })
    } else {
      goodsCatModel.id = this.editData.id;
      this.goodsService.goodsCatPatch(goodsCatModel).then(res => {
        this.message.success("修改完成!");
        this.myTable.search();
        this.editData.isVisible = false;
      })
    }


  }
}
