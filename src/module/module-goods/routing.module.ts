import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoodsListComponent } from './page/list/list.component';
import { GoodsGetComponent } from './page/get/get.component';
import { GoodsEditComponent } from './page/edit/edit.component';
import { GoodsCatComponent } from './page/cat/cat.component';

const routes: Routes = [
  { path: 'goods/list', component: GoodsListComponent },
  { path: 'goods/:id/get', component: GoodsGetComponent },
  { path: 'goods/post', component: GoodsEditComponent },
  { path: 'goods/:id/edit', component: GoodsEditComponent },
  { path: 'goodsCat/list', component: GoodsCatComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoodsRoutingModule { }
