import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { HttpService } from 'src/base/utlis/http.config';
import { CmdData } from 'src/core/module/CmdSignModel';
import { UserModel } from 'src/core/module/user/user.model';
import { RbacCmdCode } from 'src/module/module-rbac/core';
import { SysUserService } from '../../core/service/SysUserService';

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class UserEditComponent implements OnInit {

  constructor(
    private message:NzMessageService,
    private router: Router,
    private http:HttpService,
    private route: ActivatedRoute,
  ) { }

  sysUserService:SysUserService = new SysUserService(this.http);

  panelOption:NgsPanelOption={
    crumbs:[
      {
        text:'运维列表',
        action:() => {
          this.router.navigateByUrl(`/user/list`)
        }
      },
      {
        text:'运维添加'
      }
    ],
    buttons:[
      {
        text:'添加',
        action:() => {
          let myForm = this.myForm.getValue();
          let sysUserModel:UserModel = new UserModel();
          sysUserModel.avatarUrl =  myForm.avatarUrl[0];
          sysUserModel.name = myForm.name;
          sysUserModel.mobile = myForm.mobile;
          sysUserModel.roleId = myForm.roleId;
          if(this.userId){
            sysUserModel.id = this.userId;
            this.sysUserService.sysUserPatch(sysUserModel).then(res => {
              this.router.navigateByUrl("/user/list");
            });
          }else{
            this.sysUserService.sysUserPost(sysUserModel).then(res => {
              this.router.navigateByUrl("/user/list");
            });
          }
          
        }
      }
    ]
  }

  @ViewChild('myForm') myForm: NgsForm | any;

  roleCmdSign:CmdData={
    cmdCode:RbacCmdCode.RBAC_ROLE_LIST,
    returnStruct:"{id:'',name:''}"
  };
  formOption:NgsFormOption = {
    showSearch:false,
    column:3,
    components:[
      { label:'用户名称', property:'name' ,comp:'NgsFormInput',require:true },
      { label:'手机号', property:'mobile' ,comp:'NgsFormInput',require:true },
      { label:'角色', property:'roleId' ,comp:'NgsFormSelect',dataSource:this.roleCmdSign,dsLabel:'name',dsValue:'id',require:true },
      { label:'头像', property:'avatarUrl' ,comp:'NgsFormUploader',data:[],require:true }
    ]
  }

  userId:number|any;
  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      if(params.id){
        this.userId = params.id;
        let sysUserModel:UserModel = new UserModel();
        sysUserModel.id = params.id;
        this.sysUserService.sysUserGet(sysUserModel).then(res => {
          let rbacRoleBO:UserModel = res.source;
          rbacRoleBO.roleId = rbacRoleBO.rbacUserRoleBO.roleId;
          this.myForm.setValue(rbacRoleBO);
        })
      }
    })
  }

}
