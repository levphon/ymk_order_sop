import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { HttpService } from 'src/base/utlis/http.config';
import { ReqCode } from 'src/core/code';
import { CmdData } from 'src/core/module/CmdSignModel';
import { RbacRoleModel } from 'src/core/module/rbac/rbac-role.model';
import { UserModel } from 'src/core/module/user/user.model';
import { RbacCmdCode } from 'src/module/module-rbac/core';
import { SopUserCmdCode } from '../../core';
import { SysUserService } from '../../core/service/SysUserService';


@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(
    private message:NzMessageService,
    private router: Router,
    private http:HttpService,
    private modal: NzModalService
  ) { }

  sysUserService:SysUserService = new SysUserService(this.http);
  @ViewChild('myForm') myForm: NgsForm | any;
  @ViewChild('myTable') myTable: NgsTable | any;

  panelOption:NgsPanelOption={
    crumbs:[
      {
        text:'运维列表'
      }
    ],
    buttons:[
      {
        text:'添加',
        action:() => {
            this.router.navigateByUrl(`/user/post`)
        }
      }
    ]
  }

  formOption:NgsFormOption = {
    showSearch:true,
    column:4,
    components:[
      { label:'用户名称', property:'name' ,comp:'NgsFormInput',placeHolder:'用户名称' },
      { label:'手机号', property:'mobile' ,comp:'NgsFormInput',placeHolder:'手机号码' }
    ]
  }

  cmdSign:CmdData={
    cmdCode:SopUserCmdCode.SOP_USER_LIST,
    returnStruct:"{id:'',name:'',mobile:'',status:'',createdDate:'',rbacRoleBO:{id:'',name:''}}"
  };

  statusTemplate:Array<NgsTabColumnBadgeOption> = [
    { status:"DISABLED",tagLabel:'禁用',tagColor:'red' },
    { status:"NORMAL",tagLabel:'正常',tagColor:'blue' }
  ]
  
  tableOption:NgsTableOption = {
    dataSource:this.cmdSign,
    selections:[
      { text:'全部',value:null },
      { text:'启用',value:"NORMAL" },
      { text:'禁用',value:"DISABLED" }
    ],
    table:{
      columns:[
        { title:'角色名称',property:'name' },
        { title:'手机号',property:'mobile' },
        { title:'角色',property:'role',
          info:(item:UserModel) => {
            return item.rbacRoleBO.name?item.rbacRoleBO.name:'暂无';
          } 
        },
        { title:'状态',property:'status' ,badgeOption:this.statusTemplate},
        { title:'创建时间',property:'createdDate' }
      ],
      op:{
        buttons:[
          {
            text:'修改',
            action:(item:any) => {
                this.router.navigateByUrl(`user/${item.id}/edit`);
            }
          },
          {
            text:'禁用',
            hidden:(item:RbacRoleModel) => {
               return item.status == 1?true:false;
            },
            action:(item:RbacRoleModel) => {
              let rbacRoleModel:RbacRoleModel = new RbacRoleModel();
              rbacRoleModel.id = item.id;
              rbacRoleModel.status = 0;
              this.sysUserService.sysUserPatch(rbacRoleModel,ReqCode.UPDATE_STATUS).then(res => {
                this.message.success("修改完成");
                this.myTable.search();
              });
            }
          },
          {
            text:'启用',
            hidden:(item:RbacRoleModel) => {
               return item.status == 0?true:false;
            },
            action:(item:RbacRoleModel) => {
              let rbacRoleModel:RbacRoleModel = new RbacRoleModel();
              rbacRoleModel.id = item.id;
              rbacRoleModel.status = 1;
              this.sysUserService.sysUserPatch(rbacRoleModel,ReqCode.UPDATE_STATUS).then(res => {
                this.message.success("修改完成");
                this.myTable.search();
              });
            }
          },
          {
            text:'删除',
            action:(item:any) => {
              this.modal.create({
                nzTitle: '是否删除该用户',
                nzContent: '删除后无法恢复！',
                nzClosable: false,
                nzOnOk: () => {
                  this.sysUserService.sysUserDel(item).then(res => {
                    this.message.success("删除完成");
                    this.myTable.search();
                  });
                  
                }
              });
            }
          },
        ]
      }
    }
  }

  ngOnInit() {
  }

  onSearch(event:any){
    this.myTable.search(event);
  }

}
