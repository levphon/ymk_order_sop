import { NgModule } from '@angular/core';
import { NgsModule } from 'src/base/common/ngs';

import { UserEditComponent } from './page/edit/edit.component';
import { UserGetComponent } from './page/get/get.component';
import { UserListComponent } from './page/list/list.component';
import { UserRoutingModule } from './routing.module';

import { CommonModule } from '@angular/common';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';


const DIRECTIVES = [UserEditComponent, UserGetComponent, UserListComponent]

@NgModule({
  declarations: DIRECTIVES,
  exports: DIRECTIVES,
  imports:
    [
      UserRoutingModule,
      NgsModule,
      CommonModule,
      NzEmptyModule,
      NzModalModule,
      NzCheckboxModule,
      FormsModule
    ],
  entryComponents: DIRECTIVES,
  providers: []
})
export class UserModule { }
