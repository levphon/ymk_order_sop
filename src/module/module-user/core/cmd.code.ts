export class SopUserCmdCode {
    public static readonly SOP_USER_LIST = "SOP_USER_LIST";
    public static readonly SOP_USER_POST = "SOP_USER_POST";
    public static readonly SOP_USER_DEL = "SOP_USER_DEL";
    public static readonly SOP_USER_GET = "SOP_USER_GET";
    public static readonly SOP_USER_PATCH = "SOP_USER_PATCH";

}