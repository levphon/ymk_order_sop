import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './page/list/list.component';
import { UserGetComponent } from './page/get/get.component';
import { UserEditComponent } from './page/edit/edit.component';

const routes: Routes = [
  { path: 'user/list', component: UserListComponent },
  { path: 'user/:id/get', component: UserGetComponent },
  { path: 'user/post', component: UserEditComponent },
  { path: 'user/:id/edit', component: UserEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
