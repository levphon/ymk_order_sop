export class PayCmdCode {
  
    public static readonly PAY_PAYMENT_GET = "PAY_PAYMENT_GET";
    public static readonly PAY_PAYMENT_LIST = "PAY_PAYMENT_LIST";
    public static readonly PAY_PAYMENT_POST = "PAY_PAYMENT_POST";
    public static readonly PAY_PAYMENT_PATCH = "PAY_PAYMENT_PATCH";
    public static readonly PAY_PAYMENT_DEL = "PAY_PAYMENT_DEL";

    public static readonly PAY_PAYMENT_SYNC = "PAY_PAYMENT_SYNC";


    public static readonly PAY_DIVIDE_GET = "PAY_DIVIDE_GET";
    public static readonly PAY_DIVIDE_LIST = "PAY_DIVIDE_LIST";
    public static readonly PAY_DIVIDE_POST = "PAY_DIVIDE_POST";
    public static readonly PAY_DIVIDE_PATCH = "PAY_DIVIDE_PATCH";
    public static readonly PAY_DIVIDE_DEL = "PAY_DIVIDE_DEL";

    public static readonly PAY_WITHDRAW_GET = "PAY_WITHDRAW_GET";
    public static readonly PAY_WITHDRAW_LIST = "PAY_WITHDRAW_LIST";
    public static readonly PAY_WITHDRAW_POST = "PAY_WITHDRAW_POST";
    public static readonly PAY_WITHDRAW_PATCH = "PAY_WITHDRAW_PATCH";
    public static readonly PAY_WITHDRAW_DEL = "PAY_WITHDRAW_DEL";

}