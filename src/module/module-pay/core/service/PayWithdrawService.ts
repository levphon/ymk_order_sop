import { HttpService } from "src/base/utlis/http.config"
import { CmdSignModel } from "src/core/module/CmdSignModel";
import { PayCmdCode } from "../cmd.code";

export class PayWithdrawService {

    constructor(private http :HttpService) {
    }

    public payWithdrawGet(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = PayCmdCode.PAY_WITHDRAW_GET;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public payWithdrawList(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = PayCmdCode.PAY_WITHDRAW_LIST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public payWithdrawPatch(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = PayCmdCode.PAY_WITHDRAW_PATCH;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public payWithdrawPost(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = PayCmdCode.PAY_WITHDRAW_POST;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }

    public payWithdrawDel(reqData:any,reqCode?:string):Promise<any>{
        let cmdSign = new CmdSignModel();
        cmdSign.cmdCode = PayCmdCode.PAY_WITHDRAW_DEL;
        cmdSign.reqCode = reqCode;
        cmdSign.source = reqData;
        return this.http.exe(cmdSign);
    }
    
}