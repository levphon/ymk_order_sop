import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { HttpService } from 'src/base/utlis/http.config';
import { CmdData } from 'src/core/module/CmdSignModel';
import { MemberFinanceModel } from 'src/core/module/member/member-finance.model';
import { PayDivideModel } from 'src/core/module/pay/payDivide.model';
import { PayWithdrawModel } from 'src/core/module/pay/payWithdraw.model';
import { MemberService } from 'src/module/module-member/core/service/MemberService';
import { PayCmdCode } from 'src/module/module-pay/core';
import { PayWithdrawService } from 'src/module/module-pay/core/service/PayWithdrawService';


@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class PayWithdrawListComponent implements OnInit {

  constructor(
    private message:NzMessageService,
    private router: Router,
    private http:HttpService,
    private modal: NzModalService
  ) { }

  payWithdrawService:PayWithdrawService = new PayWithdrawService(this.http);
  memberService:MemberService = new MemberService(this.http);
  @ViewChild('myTable') myTable: NgsTable | any;

  panelOption:NgsPanelOption={
    crumbs:[
      {
        text:'提现列表'
      }
    ],
    buttons:[
    ]
  }

  formOption:NgsFormOption = {
    column:4,
    showSearch:true,
    components:[
      {
        label:'会员手机号',
        property:'mobile',
        comp:'NgsFormInput'
      }
    ]
  }

  cmdSign:CmdData={
    cmdCode:PayCmdCode.PAY_WITHDRAW_LIST
  };

  statusTemplate:Array<NgsTabColumnBadgeOption> = [
    { status:1,tagLabel:'已完成',tagColor:'#52c41a' },
    { status:2,tagLabel:'待处理',tagColor:'#999' },
    { status:3,tagLabel:'已关闭',tagColor:'#ff4d4f' }
  ]
  
  tableOption:NgsTableOption = {
    dataSource:this.cmdSign,
    selections:[
      { text:'全部',value:null },
      { text:'待处理',value:2 },
      { text:'已完成',value:1 },
      { text:'已关闭',value:3 }
    ],
    table:{
      columns:[
        { title:'业务类型',property:'payNo',
          info:(item:PayWithdrawModel) => {
            return "会员提现";
          },
          subOption:(item:PayWithdrawModel) => {
            return "提现";
          },
        },
        { title:'下单会员',property:'orderMember',
          info:(item:PayWithdrawModel) => {
              return item.memberBO.name;
          },
          subOption:(item:PayWithdrawModel) => {
              return item.memberBO.mobile;
          }
        },
        { title:'分账金额',property:'withdrawAmount'},
        { title:'状态',property:'status' ,badgeOption:this.statusTemplate},
        { title:'创建时间',property:'createdDate' }
      ]
    }
  }

  ngOnInit() {
    
  }

  onSearch(event:any){
    this.myTable.search(event);
  }

}
