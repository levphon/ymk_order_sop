import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { HttpService } from 'src/base/utlis/http.config';
import { CmdData } from 'src/core/module/CmdSignModel';
import { PayPaymentModel } from 'src/core/module/pay/payPayment.model';
import { PayCmdCode } from '../../core';
import { PayPaymentService } from '../../core/service/PayPaymentService';


@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class PayPaymentListComponent implements OnInit {

  constructor(
    private message:NzMessageService,
    private router: Router,
    private http:HttpService,
    private modal: NzModalService
  ) { }

  payPaymentService:PayPaymentService = new PayPaymentService(this.http);
  @ViewChild('myForm') myForm: NgsForm | any;
  @ViewChild('myTable') myTable: NgsTable | any;

  panelOption:NgsPanelOption={
    crumbs:[
      {
        text:'支付列表'
      }
    ],
    buttons:[
      {
        text:'导出',
        action:() => {
           
        }
      }
    ]
  }

  
  formOption:NgsFormOption = {
    showSearch:true,
    column:4,
    components:[
      { label:'订单编号', property:'title', placeHolder:'会员名称', comp:'NgsFormInput', },
      { label:'订单分类', property:'type',comp:'NgsFormSelect',dataSource:[{id:1,name:'正常订单'},{id:2,name:'服务订单'},{id:3,name:'定制订单'}],dsLabel:'name',dsValue:'id' },
    ]
  }

  cmdSign:CmdData={
    cmdCode:PayCmdCode.PAY_PAYMENT_LIST
  };

  statusTemplate:Array<NgsTabColumnBadgeOption> = [
    { status:1,tagLabel:'已完成',tagColor:'#52c41a' },
    { status:2,tagLabel:'待支付',tagColor:'#999' },
    { status:3,tagLabel:'已关闭',tagColor:'#faad14' },
    { status:4,tagLabel:'支付失败',tagColor:'#ff4d4f' }
  ]
  
  tableOption:NgsTableOption = {
    dataSource:this.cmdSign,
    selections:[
      { text:'全部',value:null },
      { text:'待支付',value:2 },
      { text:'已完成',value:1 },
      { text:'已关闭',value:3 }
    ],
    table:{
      columns:[
        { title:'订单编号',property:'payNo',
          click:(item:PayPaymentModel) => {

          },
          subOption:(item:PayPaymentModel) => {
            return "订单编号:"+item.ordersBO.orderNo;
          },
        },
        
        { title:'下单会员',property:'orderMember',width:150,
          click:(item:PayPaymentModel) => {
            
          },
          info:(item:PayPaymentModel) => {
              return item.orderMemberBO.name;
          },
          subOption:(item:PayPaymentModel) => {
              return item.orderMemberBO.mobile;
          }
        },
        
        { title:'支付金额',property:'amount',width:150,

          subOption:(item:PayPaymentModel) => {
          if(item.payType == 1){
            return "支付宝支付";
          }
          return "微信支付";
          },
        },
        { title:'状态',property:'status' ,badgeOption:this.statusTemplate,width:150},
        { title:'备注',property:'desc',width:220 },
        { title:'创建时间',property:'createdDate',width:120 }
      ],
      
      op:{
        width:300,
        buttons:[
          {
            text:'订单详情',
            action:(item:PayPaymentModel) => {

            }
          }
        ],
        groupButtons:[
          {
            text:'支付',
            buttons:[
              {
                text:'支付同步',
                action:(item:PayPaymentModel) => {
                    let payPaymentModel:PayPaymentModel = new PayPaymentModel();
                    payPaymentModel.id = item.id;
                    this.payPaymentService.payPaymentSync(payPaymentModel).then(res => {
                      this.message.success("同步成功");
                      this.myTable.search();
                    });
                }
              }
            ],
          },
          {
            text:'更多',
            buttons:[
              {
                text:'删除订单',
                action:(item:PayPaymentModel) => {
                    let payPaymentModel:PayPaymentModel = new PayPaymentModel();
                    payPaymentModel.id = item.id;
                    this.payPaymentService.payPaymentSync(payPaymentModel).then(res => {
                      this.message.success("修改成功");
                      this.myTable.search();
                    });
                }
              }
            ],
          }
        ]
      },
      
    }
  }

  ngOnInit() {
  }

  onSearch(event:any){
    this.myTable.search(event);
  }

}
