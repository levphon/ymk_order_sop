import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsFormOption, NgsPanelOption } from 'src/base/common/ngs';
import { NgsForm } from 'src/base/common/ngs/form/ngs-form-module';
import { NgsTable } from 'src/base/common/ngs/table/ngs-table-module';
import { NgsTabColumnBadgeOption, NgsTableOption } from 'src/base/common/ngs/table/ngs-table.config';
import { HttpService } from 'src/base/utlis/http.config';
import { CmdData } from 'src/core/module/CmdSignModel';
import { PayDivideModel } from 'src/core/module/pay/payDivide.model';
import { PayPaymentModel } from 'src/core/module/pay/payPayment.model';
import { PayCmdCode } from 'src/module/module-pay/core';
import { PayDivideService } from 'src/module/module-pay/core/service/PayDivideService';




@Component({
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class PayDivideListComponent implements OnInit {

  constructor(
    private message:NzMessageService,
    private router: Router,
    private http:HttpService,
    private modal: NzModalService
  ) { }

  payPaymentService:PayDivideService = new PayDivideService(this.http);
  @ViewChild('myForm') myForm: NgsForm | any;
  @ViewChild('myTable') myTable: NgsTable | any;

  panelOption:NgsPanelOption={
    crumbs:[
      {
        text:'支付列表'
      }
    ],
    buttons:[
      {
        text:'导出',
        action:() => {
           
        }
      }
    ]
  }

  
  formOption:NgsFormOption = {
    showSearch:true,
    column:4,
    components:[
      { label:'订单编号', property:'title' ,comp:'NgsFormInput', },
      { label:'订单分类', property:'type' ,comp:'NgsFormSelect',dataSource:[{id:1,name:'正常订单'},{id:2,name:'服务订单'},{id:3,name:'定制订单'}],dsLabel:'name',dsValue:'id' },
    ]
  }

  cmdSign:CmdData={
    cmdCode:PayCmdCode.PAY_DIVIDE_LIST
  };

  statusTemplate:Array<NgsTabColumnBadgeOption> = [
    { status:1,tagLabel:'已分账',tagColor:'#52c41a' },
    { status:2,tagLabel:'待分帐',tagColor:'#999' },
    { status:3,tagLabel:'已关闭',tagColor:'#ff4d4f' }
  ]
  
  tableOption:NgsTableOption = {
    dataSource:this.cmdSign,
    selections:[
      { text:'全部',value:null },
      { text:'待分帐',value:2 },
      { text:'已分账',value:1 },
      { text:'已关闭',value:3 }
    ],
    table:{
      columns:[
        { title:'业务类型',property:'payNo',
          info:(item:PayDivideModel) => {
            return "支付下单";
          },
          subOption:(item:PayDivideModel) => {
            return "分账";
          },
        },
        { title:'订单编号',property:'payNo',
          info:(item:PayDivideModel) => {
            return item.ordersBO.orderNo;
          },
          subOption:(item:PayDivideModel) => {
            return item.payPaymentBO.payNo;
          },
        },
        
        { title:'下单会员',property:'orderMember',
          info:(item:PayDivideModel) => {
              return item.memberBO.name;
          },
          subOption:(item:PayDivideModel) => {
              return item.memberBO.mobile;
          }
        },
        
        { title:'分账金额',property:'amount',width:150},
        { title:'状态',property:'status' ,badgeOption:this.statusTemplate,width:150},
        { title:'创建时间',property:'createdDate',width:120 }
      ],
      
      op:{
        width:300,
        buttons:[
          {
            text:'分账',
            action:(item:PayDivideModel) => {
              console.log(item);
                this.router.navigateByUrl(`pay/divide/${item.id}/edit`);
            }
          }
        ],
        groupButtons:[
          {
            text:'更多',
            buttons:[
              {
                text:'删除订单',
                action:(item:PayDivideModel) => {
                    let payPaymentModel:PayDivideModel = new PayDivideModel();
                    payPaymentModel.id = item.id;
                    
                }
              }
            ],
          }
        ]
      },
      
    }
  }

  ngOnInit() {
  }

  onSearch(event:any){
    this.myTable.search(event);
  }

}
