import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageModule, NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NgsDescOption, NgsPanelOption } from 'src/base/common/ngs';
import { HttpService } from 'src/base/utlis/http.config';
import { ReqCode } from 'src/core/code';
import { PayDivideModel } from 'src/core/module/pay/payDivide.model';
import { PayPaymentModel } from 'src/core/module/pay/payPayment.model';
import { PayDivideService } from 'src/module/module-pay/core/service/PayDivideService';


@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class PayDivideEditComponent implements OnInit {

  constructor(
    private router: Router,
    private http: HttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private modal: NzModalService
  ) { }

  payDivideService: PayDivideService = new PayDivideService(this.http);


  panelOption: NgsPanelOption = {
    crumbs: [
      {
        text: '分账列表',
        action: () => {
          this.router.navigateByUrl(`/pay/divide/list`)
        }
      },
      {
        text: '分账详情'
      }
    ]
  }

  payDivideId: number = 0;
  payDivideModel: PayDivideModel = new PayDivideModel();

  baseDescOption: NgsDescOption = {
    dataSource: this.payDivideModel,
    descItemOption: [
      {
        key: '分账状态',
        info: (item: PayDivideModel) => {
          if (item.status === 1) {
            return '已分账';
          }
          if (item.status === 2) {
            return '待分账';
          }
          return '已关闭';
        }
      },
      {
        key: '订单类型',
        info: (item: PayDivideModel) => {
          if (!item.ordersBO) {
            return "";
          }
          if (item.ordersBO.type === 1) {
            return "常规订单";
          }
          if (item.ordersBO.type === 2) {
            return "服务订单";
          }
          return "定制订单";
        }
      },
      {
        key: '业务类型',
        info: (item: PayDivideModel) => {
          return "支付下单";
        }
      },
      {
        key: '下单人',
        info: (item: PayDivideModel) => {
          return item.orderMemberBO ? item.orderMemberBO.name : "";
        }
      },
      {
        key: '手机号码',
        info: (item: PayDivideModel) => {
          return item.orderMemberBO ? item.orderMemberBO.mobile : "";
        }
      },
      {
        key: '分账金额',
        value: 'amount'
      },
      {
        key: '订单编号',
        info: (item: PayDivideModel) => {
          return item.ordersBO ? item.ordersBO.orderNo : "";
        }
      },
      {
        key: '订单时间',
        info: (item: PayDivideModel) => {
          return item.ordersBO ? item.ordersBO.createdDate + "" : "";
        }
      },
      {
        key: '创建时间',
        value: 'createdDate'
      }
    ]
  }

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      if (params.id) {
        this.payDivideId = params.id;
        let payDivideService = new PayDivideModel();
        payDivideService.id = params.id;
        this.payDivideService.payDivideGet(payDivideService).then(res => {
          let pryDivideBO = res.source;
          this.payDivideModel = res.source;
          setTimeout(() => {
            this.baseDescOption.dataSource = pryDivideBO;
          }, 100);

        });
      }
    })
  }

  /**
   * 获取表格文案
   */
  getOrdersMember(type: number): string {
    if (!this.payDivideModel || !this.payDivideModel.memberBO) {
      return "";
    }
    if (type === 1) {
      return this.payDivideModel.memberBO.name;
    }
    if (type === 2) {
      return this.payDivideModel.memberBO.mobile;
    }
    return "";
  }

  /**
   * 分账
   */
   onDivide() {
    console.log("aaa")
    this.modal.create({
      nzTitle: '是否进行分账',
      nzContent: '分账后无法恢复！',
      nzClosable: false,
      nzOnOk: () => {
        let payPaymentBO: PayPaymentModel = new PayPaymentModel();
        payPaymentBO.id = this.payDivideId;

        let that = this;
        this.payDivideService.payDividePatch(payPaymentBO, ReqCode.PAY_DIVIDE_PATCH).then(res => {
          that.message.success("分账成功！");
          that.router.navigateByUrl("/pay/divide/list");
        })

      }
    })
  }


}
