import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PayDivideEditComponent } from './page/divide/edit/edit.component';
import { PayDivideListComponent } from './page/divide/list/list.component';
import { PayPaymentListComponent } from './page/list/list.component';
import { PayWithdrawListComponent } from './page/withdraw/list/list.component';


const routes: Routes = [
  { path: "pay/list", component: PayPaymentListComponent },
  { path: "pay/divide/list", component: PayDivideListComponent },
  { path: "pay/divide/:id/edit", component: PayDivideEditComponent },
  { path: "pay/withdraw/list", component:PayWithdrawListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PayPaymentRoutingModule { }
