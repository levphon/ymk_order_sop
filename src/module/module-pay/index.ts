import { NgModule } from '@angular/core';
import { NgsModule } from 'src/base/common/ngs';


import { PayPaymentRoutingModule } from './routing.module';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzButtonModule } from 'ng-zorro-antd/button';

import { CommonModule } from '@angular/common';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzEmptyModule } from 'ng-zorro-antd/empty';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { FormsModule } from '@angular/forms';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzResultModule } from 'ng-zorro-antd/result';
import { PayPaymentListComponent } from './page/list/list.component';
import { PayDivideListComponent } from './page/divide/list/list.component';
import { PayDivideEditComponent } from './page/divide/edit/edit.component';
import { PayWithdrawListComponent } from './page/withdraw/list/list.component';


const DIRECTIVES = [PayPaymentListComponent,PayDivideListComponent,PayDivideEditComponent,PayWithdrawListComponent]

@NgModule({
  declarations: DIRECTIVES,
  exports: DIRECTIVES,
  imports:
    [
      PayPaymentRoutingModule,
      NgsModule,
      CommonModule,
      NzEmptyModule,
      NzModalModule,
      NzCheckboxModule,
      FormsModule,
      NzTabsModule,
      NzTableModule,
      NzButtonModule,
      NzDividerModule,
      NzToolTipModule,
      NzIconModule,
      NzInputNumberModule,
      NzResultModule
    ],
  entryComponents: DIRECTIVES,
  providers: []
})
export class PayPaymentModule { }
