FROM nginx:1.17

COPY ./dist/bs-admin /usr/share/nginx/html

COPY ./nginx-angular.conf /etc/nginx/conf.d/default.conf